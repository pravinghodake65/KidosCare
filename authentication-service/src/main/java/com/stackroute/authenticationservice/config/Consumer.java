package com.stackroute.authenticationservice.config;
import com.stackroute.authenticationservice.entity.User;
import com.stackroute.authenticationservice.service.impl.UserServiceImpl;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RabbitListener(queues="user_queue")
    public void getUserDtoFromRabbitMq(User user)
    {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(user);
        System.out.println("Got the details");
    }
}
