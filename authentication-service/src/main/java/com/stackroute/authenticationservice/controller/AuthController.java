package com.stackroute.authenticationservice.controller;

import com.stackroute.authenticationservice.dto.JwtResponse;
import com.stackroute.authenticationservice.dto.LoginDto;
import com.stackroute.authenticationservice.dto.ResetPasswordDto;
import com.stackroute.authenticationservice.dto.SignUpDto;
import com.stackroute.authenticationservice.entity.User;
import com.stackroute.authenticationservice.repository.UserRepository;
import com.stackroute.authenticationservice.security.JwtTokenProvider;
import com.stackroute.authenticationservice.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;


@RestController
@SecurityRequirement(name = "javainuseapi")
@RequestMapping("/api/v1/auth")
public class AuthController {

	/**
	 * Requirements for login
	 */
	@Autowired
	 AuthenticationManager authenticationManager;

	/**
	 * Requirements for signup
	 */
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserService userService;

	/**
	 * JWT
	 */
	@Autowired
	 JwtTokenProvider jwtTokenProvider;

	@PostMapping("/login")
	public ResponseEntity<JwtResponse> loginUser(@RequestBody LoginDto loginDto) {

		return new ResponseEntity<JwtResponse>(userService.loginUser(loginDto), HttpStatus.OK);

	}

	@PostMapping("/signup")
	public ResponseEntity<?> signUpUser(@RequestBody SignUpDto signUpDto) {

		/**
		 * Check for username already exists or not
		 */
		if (userRepository.existsByUsername(signUpDto.getUsername())) {
			return new ResponseEntity<>("username is already taken", HttpStatus.BAD_REQUEST);
		}

		/**
		 * Check for email already exists or not
		 */
		if (userRepository.existsByEmail(signUpDto.getEmail())) {
			return new ResponseEntity<String>("email is already taken", HttpStatus.BAD_REQUEST);
		}

		/**
		 * create User object and set values using signUpDto
		 */
		User user = new User();
		user.setUsername(signUpDto.getUsername());
		user.setEmail(signUpDto.getEmail());
		// encode password
		user.setPassword(passwordEncoder.encode(signUpDto.getPassword()));
		user.setRole(signUpDto.getRole());

		userRepository.save(user);
		return new ResponseEntity<String>("Sign up is successful", HttpStatus.OK);

	}


	@PutMapping("/forgetPassword")
	public ResponseEntity<?> forgetResetPassword(@RequestBody ResetPasswordDto resetPasswordDto) {

		/**
		 * Check for email & username exists or not
		 */
		if (userRepository.existsByEmail(resetPasswordDto.getEmail()) && userRepository.existsByUsername(resetPasswordDto.getUsername())) {

			Optional<User> user = userRepository.findByEmail(resetPasswordDto.getEmail());
			if(resetPasswordDto.getPassword().equals(resetPasswordDto.getConfirmPassword())) {
				user.get().setPassword(passwordEncoder.encode(resetPasswordDto.getPassword()));
				userRepository.save(user.get());
				return new ResponseEntity<>("Your password is updated!", HttpStatus.OK);
			}

		}
		return new ResponseEntity<>("Email id or username does not exist!", HttpStatus.BAD_REQUEST);

		//return null;
	}



}
