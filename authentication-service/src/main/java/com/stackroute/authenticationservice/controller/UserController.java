package com.stackroute.authenticationservice.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.stackroute.authenticationservice.security.SecurityConfig.SECURITY_CONFIG_NAME;

@RestController
@RequestMapping("/api/v1")
@SecurityRequirement(name = SECURITY_CONFIG_NAME)
public class UserController {
	
	@PreAuthorize("hasRole('ROLE_SELLER') or hasRole('ROLE_BUYER')")
	@GetMapping("/hello")
	public String hello() {
		return "AnyOne can access this";
	}
	
	@PreAuthorize("hasRole('ROLE_SELLER')")
	@PostMapping("/seller")
	public String sellerAccess() {
		return "Only Seller can view this";
	}
	
	@PreAuthorize("hasRole('ROLE_BUYER')")
	@PostMapping("/buyer")
	public String buyerAccess() {
		return "Only buyer can view this";
	}

}
