package com.stackroute.authenticationservice.exception;

public class UserAlreadyExistsException extends RuntimeException{

    private String message;

    public UserAlreadyExistsException() {
        super();
    }

    public UserAlreadyExistsException(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
