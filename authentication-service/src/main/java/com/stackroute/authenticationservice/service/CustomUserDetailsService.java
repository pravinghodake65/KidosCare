package com.stackroute.authenticationservice.service;


import com.stackroute.authenticationservice.entity.User;
import com.stackroute.authenticationservice.exception.UserNameNotFoundException;
import com.stackroute.authenticationservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {

        User user = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail).orElseThrow(
                () -> new UserNameNotFoundException("User not found with username or email " + usernameOrEmail));

        return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),Arrays.asList(new SimpleGrantedAuthority(user.getRole())));
    }


}
