package com.stackroute.authenticationservice.service;

import com.stackroute.authenticationservice.dto.JwtResponse;
import com.stackroute.authenticationservice.dto.LoginDto;
import com.stackroute.authenticationservice.entity.User;


public interface UserService {
	
	JwtResponse loginUser(LoginDto loginDto);
	User save(User user);

}
