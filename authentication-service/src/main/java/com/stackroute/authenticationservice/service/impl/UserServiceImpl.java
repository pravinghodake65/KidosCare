package com.stackroute.authenticationservice.service.impl;

import com.stackroute.authenticationservice.dto.JwtResponse;
import com.stackroute.authenticationservice.dto.LoginDto;

import com.stackroute.authenticationservice.entity.User;
import com.stackroute.authenticationservice.exception.UserAlreadyExistsException;
import com.stackroute.authenticationservice.repository.UserRepository;
import com.stackroute.authenticationservice.security.JwtTokenProvider;
import com.stackroute.authenticationservice.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

	 static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * @description : Logs in a user
	 * @param loginDto : Takes LoginDto as parameter
	 * @return jwtResponse : Returns JwtResponse
	 */
	@Override
	public JwtResponse loginUser(LoginDto loginDto) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginDto.getUsernameOrEmail(), loginDto.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);

		String token = jwtTokenProvider.generateToken(authentication);

		return new JwtResponse(token);
	}

	/**
	 * @description : Add user
	 * @param user : Takes user as parameter
	 * @return jwtResponse : Returns User
	 */
	@Override
	public User save(User user) {
		if(userRepository.findByEmail(user.getEmail()).isPresent())
		{
			throw new UserAlreadyExistsException();
		}
		System.out.println(user);
		return userRepository.save(user);
	}



}
