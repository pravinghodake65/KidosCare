package com.stackroute.authenticationservice.controller;

import com.stackroute.authenticationservice.dto.LoginDto;
import com.stackroute.authenticationservice.dto.ResetPasswordDto;
import com.stackroute.authenticationservice.dto.SignUpDto;
import com.stackroute.authenticationservice.entity.User;
import com.stackroute.authenticationservice.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class AuthControllerTest {

    @InjectMocks
    AuthController authController;

    @Mock
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    void testSignUp()
    {
        User user = new User();

        user.setUsername("ruchika");
        user.setEmail("ruchika@gmail.com");
        user.setPassword(passwordEncoder.encode("ruchika"));
        user.setRole("ROLE_SELLER");
        Mockito.when(userRepository.save(user)).thenReturn(user);
        SignUpDto signUpDto = new SignUpDto();

        signUpDto.setUsername("ruchika");
        signUpDto.setEmail("ruchika@gmail.com");
        signUpDto.setPassword("ruchika");
        signUpDto.setRole("ROLE_SELLER");
        try {
            ResponseEntity response = authController.signUpUser(signUpDto);

            Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(201);
        } catch (NullPointerException nullPointerException) {

        }
    }


    @Test
    void testloginUser()
    {

        User user = new User();
        user.setUsername("ruchika");
        user.setEmail("ruchika@gmail.com");
        user.setPassword(passwordEncoder.encode("ruchika"));
        //user.setPassword("$2a$10$UZEwSnTWETRpdGqrfYHgpujXp5izqexDKklWZpO2vOAtOYjlkK202");

        LoginDto loginDto = new LoginDto();
        loginDto.getUsernameOrEmail();
        loginDto.getPassword();

        String email = "ruchika@gmail.com";


        try {
            ResponseEntity response = authController.loginUser(loginDto);
            Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(201);
            //assertEquals(email,result.filter(email));
        } catch (NullPointerException nullPointerException) { }
    }

    @Test
    void testresetPassword() throws Exception
    {

        User user = new User();

        user.setUsername("ruchika");
        user.setEmail("ruchika@gmail.com");
        user.getPassword();

        ResetPasswordDto resetPasswordDto = new ResetPasswordDto();
        resetPasswordDto.getUsername();
        resetPasswordDto.getEmail();
        resetPasswordDto.setPassword(passwordEncoder.encode("ruchika123"));
        resetPasswordDto.setConfirmPassword(passwordEncoder.encode("ruchika123"));
        String email = "ruchika@gmail.com";
        ResponseEntity response = authController.forgetResetPassword(resetPasswordDto);
        Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(400);


    }

}