package com.stackroute.authenticationservice.service.impl;

import com.stackroute.authenticationservice.dto.JwtResponse;
import com.stackroute.authenticationservice.dto.LoginDto;
import com.stackroute.authenticationservice.dto.SignUpDto;
import com.stackroute.authenticationservice.entity.User;
import com.stackroute.authenticationservice.exception.JwtException;
import com.stackroute.authenticationservice.repository.UserRepository;
import com.stackroute.authenticationservice.security.JwtTokenProvider;
import com.stackroute.authenticationservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserServiceImplTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    UserServiceImpl userService;
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Test
    public void testAddUserDetails() {
        try {
            User user = new User(1, "ruchika", "ruchika@gmail.com", "Mg@12345", "ROLE_SELLER");
            SignUpDto signUpDto = new SignUpDto();
            when(userRepository.save(user)).thenReturn(user);
            signUpDto.setEmail("ruchika@gmail.com");
            assertEquals(user.getEmail(), signUpDto.getEmail());
        } catch (NullPointerException nullPointerException) {

        }
    }
    @Test
    void testLoginAndGenerateJwtToken() {
        LoginDto loginDto = new LoginDto();
        loginDto.setUsernameOrEmail("alexa");
        loginDto.setPassword("$2a$10$lc8rfvdNIC2zFNxRc0YB5uaakRPVyJgf1aavMP8g2hfW4wgiED9hK");
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbGV4YUBnbWFpbC5jb20iLCJpYXQiOjE2NzAzOTM1NDUsImV4cCI6MTY3MDgyNTU0NX0.l7nPpJKbY-EiuLcryfe2jthCl5zrCGw7UrmzPrFnvxAzydo0junjoqI8Qw9w7bQeqM8YCeXg7LetqiCfNnLX-A";
        JwtResponse jwtResponse = new JwtResponse();
        jwtResponse.setAccessToken(token);
        when(userService.loginUser(loginDto)).thenReturn(jwtResponse);
        assertNotNull(userService.loginUser(loginDto));
    }
    @Test
    void itShouldThrowJwtExceptionWithInvalidToken() {
        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ";
        assertThrows(JwtException.class, () -> {
            jwtTokenProvider.validateToken(token);
        });
    }
    @Test
    void itShouldThrowUnparsableToken() {
        String token = "Invalid token";
        assertThrows(JwtException.class, () -> {
            jwtTokenProvider.validateToken(token);
        });
    }


}