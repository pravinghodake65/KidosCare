package com.stackroute.cartservice.config;


import com.stackroute.cartservice.service.CartServiceImpl;

import com.stackroute.rabbitmq.domain.ProductDTO;
import com.stackroute.rabbitmq.domain.UserDto;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private CartServiceImpl service;


    @RabbitListener(queues =  ConsumerConfig.cart_queue)
    public void consumeLoanDetailsFromQueue(UserDto userDto) {
        System.out.println("User details recieved from queue : " + userDto);
        service.createCartForNewUser(userDto);
    }

    @RabbitListener(queues =  ConsumerConfig.product_queue)
    public void producerLoanDetailsFromQueue(ProductDTO productDTO) throws Exception {
        System.out.println("Product details recieved from queue : " + productDTO);
    }

}
