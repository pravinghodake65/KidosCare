package com.stackroute.cartservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConsumerConfig {

    public static final String cart_queue  = "cart_queue";
    public static final String USER_ROUTING_KEY = "user_routing_key";
    public static final String EXCHANGE = "exchange";
    public static final String product_queue = "product_queue";
    public static final String PRODUCT_ROUTING_KEY = "product_routing_key";

    @Bean
    public Queue cartQueue() {
        return new Queue(cart_queue,false, false,false);

    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    public Binding userBinding(TopicExchange exchange,Queue cartQueue) {
        return BindingBuilder.bind(cartQueue).to(exchange).with(USER_ROUTING_KEY);
    }

    @Bean
    public Queue productQueue() {
        return new Queue(product_queue, false);
    }

    @Bean
    public Binding productBinding(Queue productQueue, TopicExchange exchange) {
        return BindingBuilder.bind(productQueue()).to(exchange()).with(PRODUCT_ROUTING_KEY);
    }
    @Bean
    public Jackson2JsonMessageConverter converter()
    {
        return new  Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}