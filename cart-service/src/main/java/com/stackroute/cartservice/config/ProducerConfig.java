package com.stackroute.cartservice.config;

import org.springframework.amqp.core.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProducerConfig {

    public static final String PAYMENT_QUEUE = "payment_queue";
    public static final String ORDER_QUEUE = "order_queue";
    public static final String EXCHANGE = "cartExchange";
    @Bean
    public Queue producerPaymentQueue() {
        return new Queue(PAYMENT_QUEUE);
    }
    @Bean
    public Queue producerOrderQueue() {
        return new Queue(ORDER_QUEUE);
    }


    @Bean
    public FanoutExchange producerExchange() {
        return new FanoutExchange(EXCHANGE);
    }


    @Bean
    public Binding producerPaymentBinding(FanoutExchange producerExchange,Queue producerPaymentQueue) {
        return BindingBuilder.bind(producerPaymentQueue).to(producerExchange);
    }

    @Bean
    public Binding producerOrderBinding(FanoutExchange producerExchange,Queue producerOrderQueue) {
        return BindingBuilder.bind(producerOrderQueue).to(producerExchange);
    }

 /*   @Bean
    public MessageConverter Prodconverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate Prodtemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(Prodconverter());
        return rabbitTemplate;
    }*/


}