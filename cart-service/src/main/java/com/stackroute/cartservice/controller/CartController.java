package com.stackroute.cartservice.controller;

import com.stackroute.cartservice.dto.CartDto;

import com.stackroute.cartservice.service.CartService;

import com.stackroute.cartservice.util.CartConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * To Create new Cart for Buyer
     */
    @PostMapping("/addToCart")
    public ResponseEntity<CartDto> addProductToCart(@RequestParam("userEmail") String email, @RequestParam("productId") String productId) throws Exception {
        return new ResponseEntity<CartDto>(cartService.addToCart(email, productId), HttpStatus.CREATED);
    }


    /**
     * To Get the Cart details from database
     */
    @GetMapping("/getByUserId/{userEmail}")
    public ResponseEntity<CartDto> getCartDetailsByUserId(@PathVariable(value = "userEmail") String userEmail) {
        return new ResponseEntity<CartDto>(cartService.getCartByUserId(userEmail), HttpStatus.OK);
    }


    /**
     * To Get the Cart details from database
     */
    @GetMapping("/getByCartId/{cartId}")
    public ResponseEntity<CartDto> getCartDetailsByCartId(@PathVariable(value = "cartId") String cartId) {
        return new ResponseEntity<CartDto>(cartService.getCartByCartId(cartId), HttpStatus.OK);
    }


    /**
     * To Get all cart details from database
     */
    @GetMapping("/getAllCarts")
    public ResponseEntity getAllProduct() {
        return new ResponseEntity(cartService.getAllCart(), HttpStatus.OK);
    }


    /**
     * To Delete the product from database
     */
    @DeleteMapping("/removeProduct/{userEmail}/{productId}")
    public ResponseEntity<CartDto> deleteProductFromCart(@PathVariable(value = "userEmail") String userEmail, @PathVariable String productId) {
        return new ResponseEntity<CartDto>(cartService.deleteProductFromCart(userEmail, productId), HttpStatus.OK);
    }


    /**
     * To Clear the cart from database
     */
    @DeleteMapping("/clearCart/{userEmail}")
    public ResponseEntity<CartDto> clearCart(@PathVariable(value = "userEmail") String userEmail) {
        return new ResponseEntity<CartDto>(cartService.clearCart(userEmail), HttpStatus.OK);
    }
}
