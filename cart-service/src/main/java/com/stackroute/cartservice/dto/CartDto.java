package com.stackroute.cartservice.dto;

import com.stackroute.cartservice.entity.Product;

import com.stackroute.rabbitmq.domain.AddressDto;
import lombok.*;

import java.util.Map;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CartDto {

    private String cartId = UUID.randomUUID().toString();
    private String userEmail;
    private Map<String, Product> cartProducts;
    private Map<String, Integer> productQuantity;
    private int totalItems;
    private float totalAmount;
    private AddressDto addressDto;

}
