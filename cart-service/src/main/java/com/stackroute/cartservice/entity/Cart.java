package com.stackroute.cartservice.entity;


import com.stackroute.rabbitmq.domain.AddressDto;
import lombok.*;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Map;
import java.util.UUID;


@Document(collection = "cart")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    @MongoId
    private String cartId = UUID.randomUUID().toString();
    private String userEmail;
    private Map<String, Product> cartProducts;
    private Map<String, Integer> productQuantity;
    private int totalItems;
    private float totalAmount;
    private AddressDto address;

}
