package com.stackroute.cartservice.entity;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Document(collection = "products")
@Data
@AllArgsConstructor
public class Product {
    @MongoId
    private String productId= UUID.randomUUID().toString();
    private String productName;
    private String productCategory;
    private String productDescription;
    private String mfgDate;
    private String expDate;
  //  private Integer productUnits;
    private Long mrpPrice;
    private Long bestPrice;
    private	String productImage;



}
