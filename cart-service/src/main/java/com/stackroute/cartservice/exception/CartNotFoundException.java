package com.stackroute.cartservice.exception;

public class CartNotFoundException extends RuntimeException{
    String message;

    public CartNotFoundException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
