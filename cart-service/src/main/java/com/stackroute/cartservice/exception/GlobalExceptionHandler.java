package com.stackroute.cartservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDate;
@RestControllerAdvice
public class GlobalExceptionHandler {


    /*
     * @description : Handles the ProductNotExistInCartException
     * @param exception, webRequest : ProductNotExistInCartException and WebRequest as Parameters
     * @return errorResponse : Customized Error Response
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     */
    @ExceptionHandler(ProductNotExistInCartException.class)
    public ResponseEntity<ErrorInfo> productNotExistInCartException(ProductNotExistInCartException exception, WebRequest webRequest){
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setTimestamp(LocalDate.now());
        errorInfo.setMessage(exception.getMessage());
        errorInfo.setDetails(webRequest.getDescription(false));
        return new ResponseEntity<>(errorInfo, HttpStatus.NOT_FOUND);

    }

    /*
     * @description : Handles the CartNotFoundException
     * @param exception, webRequest : CartNotFoundException and WebRequest as Parameters
     * @return errorResponse : Customized Error Response
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     */
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorInfo> userNotFoundException(UserNotFoundException exception, WebRequest webRequest){
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setTimestamp(LocalDate.now());
        errorInfo.setMessage(exception.getMessage());
        errorInfo.setDetails(webRequest.getDescription(false));
        return new ResponseEntity<>(errorInfo, HttpStatus.NOT_FOUND);

    }

    /*
     * @description : Handles the Exception
     * @param exception, webRequest : Exception and WebRequest as Parameters
     * @return errorResponse : Customized Error Response
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorInfo> exception(Exception exception, WebRequest webRequest){
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setTimestamp(LocalDate.now());
        errorInfo.setMessage(exception.getMessage());
        errorInfo.setDetails(webRequest.getDescription(false));
        return new ResponseEntity<>(errorInfo, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /*
     * @description : Handles the CartNotFoundException
     * @param exception, webRequest : CartNotFoundException and WebRequest as Parameters
     * @return errorResponse : Customized Error Response
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     */
    @ExceptionHandler(CartNotFoundException.class)
    public ResponseEntity<ErrorInfo> cartNotFoundException(CartNotFoundException exception, WebRequest webRequest){
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setTimestamp(LocalDate.now());
        errorInfo.setMessage(exception.getMessage());
        errorInfo.setDetails(webRequest.getDescription(false));
        return new ResponseEntity<>(errorInfo, HttpStatus.NOT_FOUND);

    }

}
