package com.stackroute.cartservice.exception;

public class ProductNotExistInCartException extends RuntimeException {

    String message;

    public ProductNotExistInCartException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
