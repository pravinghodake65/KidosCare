package com.stackroute.cartservice.repository;

import com.stackroute.cartservice.entity.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepository extends MongoRepository<Cart, String> {
   //public Cart findByUserId(String userId);
  Optional<Cart> findByUserEmail(String userEmail);
  Cart findByCartId(String cartId);
}
