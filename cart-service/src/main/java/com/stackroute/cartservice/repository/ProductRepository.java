package com.stackroute.cartservice.repository;

import com.stackroute.cartservice.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
    Map<String, Product> findByProductId(String productId);
}
