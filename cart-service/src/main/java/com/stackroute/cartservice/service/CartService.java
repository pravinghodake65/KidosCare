package com.stackroute.cartservice.service;

import com.stackroute.cartservice.dto.CartDto;

import com.stackroute.cartservice.entity.Cart;

import java.util.List;


public interface CartService {

    CartDto addToCart(String userEmail, String productId) throws Exception;

    CartDto updateCart(CartDto cartDto);

    CartDto getCartByUserId(String userEmail);

    CartDto getCartByCartId(String cartId);

    CartDto deleteProductFromCart(String userEmail, String productId);

    public CartDto clearCart(String userEmail);

    List<Cart> getAllCart();

    public Cart convertDtoToEntity(CartDto cartDto);

    public CartDto convertEntityToDto(Cart cart);

}
