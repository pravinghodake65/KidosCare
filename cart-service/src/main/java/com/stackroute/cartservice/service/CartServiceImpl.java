package com.stackroute.cartservice.service;

import com.stackroute.cartservice.config.ProducerConfig;
import com.stackroute.cartservice.dto.CartDto;

import com.stackroute.cartservice.entity.Cart;
import com.stackroute.cartservice.entity.Product;

import com.stackroute.cartservice.exception.CartNotFoundException;
import com.stackroute.cartservice.exception.UserNotFoundException;
import com.stackroute.cartservice.exception.ProductNotExistInCartException;
import com.stackroute.cartservice.repository.CartRepository;
import com.stackroute.cartservice.repository.ProductRepository;

import com.stackroute.cartservice.util.CartConstants;
import com.stackroute.rabbitmq.domain.AddressDto;
import com.stackroute.rabbitmq.domain.UserDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RabbitTemplate template;

    Log log = LogFactory.getLog(CartServiceImpl.class);

    public CartServiceImpl(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    /*
     *
     * @Description : This method is used to create cart for user
     * @param : Takes CartDto as parameter
     * @returns : Returns created CartDto
     * @throws :It throws exception if the user id is already available in the table
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */

    @Override
    public CartDto addToCart(String userEmail, String productId) {
        log.info(" Getting cart details by user id ");
        Optional<Cart> existingCart = cartRepository.findByUserEmail(userEmail);
        if (existingCart.isPresent()) {
            Cart cart = existingCart.get();
            Map<String, Integer> productQuantity = cart.getProductQuantity();
            Map<String, Product> products = cart.getCartProducts();
            AddressDto address = cart.getAddress();
            Product p = productRepository.findById(productId).
                    orElseThrow(() -> new ProductNotExistInCartException(CartConstants.PRODUCT_NOT_FOUND_MESSAGE + productId));

            if (products.containsKey(p.getProductId())) {
                int count = productQuantity.get(p.getProductId());
                productQuantity.put(p.getProductId(), count + 1);
            } else {
                products.put(p.getProductId(), p);
                productQuantity.put(p.getProductId(), 1);
            }

            cart.setUserEmail(userEmail);
            cart.setCartProducts(products);
            cart.setProductQuantity(productQuantity);
            cart.setAddress(address);

            CartDto c = updateCart(convertEntityToDto(cart));
            Cart finalCart = cartRepository.save(convertDtoToEntity(c));
            log.info("Product details added successfully");
            return sendCartToRabbitMQ(convertEntityToDto(finalCart));

        }
        log.error("User not found in db");
        throw new UserNotFoundException(CartConstants.USER_NOT_FOUND_MESSAGE+userEmail);
    }

    /*
     *
     * @Description : This method is used to update product in cart
     * @param : Takes userId and ProductDto as parameter
     * @returns : Returns updated CartDto
     * @throws :It throws Cart Not Found Exception if cart is not available for user
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    @Override
    public CartDto updateCart(CartDto cartDto) {

        Cart cart = convertDtoToEntity(cartDto);
        float totalSum = 0;
        int count = 0;
        for (Product p : cart.getCartProducts().values()) {
            totalSum += p.getBestPrice() * cart.getProductQuantity().get(p.getProductId());
            count += cart.getProductQuantity().get(p.getProductId());
        }
        cart.setTotalAmount(totalSum);
        cart.setTotalItems(count);
        log.info("Product details updated successfully");
        return convertEntityToDto(cart);
    }

    /*
     *
     * @Description : This method is used to show cart details using user id
     * @param : Takes userId as parameter
     * @returns : Returns CartDto (all the products which available in cart db)
     * @throws :It throws Cart Not Found Exception if cart is not available for user
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    @Override
    public CartDto getCartByUserId(String userEmail) {
        log.info("Getting cart details by user id ");
        Optional<Cart> cart = cartRepository.findByUserEmail(userEmail);
        if (cart.isEmpty()) {
            log.error("User not found in db");
            throw new UserNotFoundException(CartConstants.USER_NOT_FOUND_MESSAGE + userEmail);
        }
        log.info("Got cart details successfully ");
        return convertEntityToDto(cart.get());
    }

    /*
     *
     * @Description : This method is used to show cart details using cart id
     * @param : Takes cartId as parameter
     * @returns : Returns CartDto (all the products which available in cart db)
     * @throws :It throws Cart Not Found Exception if cart is not available for user
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    @Override
    public CartDto getCartByCartId(String cartId) {
        log.info(" Getting cart details by cart id ");
        Cart cart = cartRepository.findByCartId(cartId);
        if (cart == null) {
            log.error("Cart not found in db");
            throw new CartNotFoundException(CartConstants.CART_NOT_FOUND_MESSAGE + cartId);
        }
        log.info(" Got cart details successfully ");
        return convertEntityToDto(cart);
    }

    /*
     *
     * @Description : This method is used to remove product from cart
     * @param : Takes userId and productId as parameter
     * @returns :
     * @throws :It throws Cart Not Found Exception if cart is not available for user and
     *          throws Product Not Exist In Cart Exception if product is not available in cart
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    @Override
    public CartDto deleteProductFromCart(String userEmail, String productId) {
        log.info("Getting Cart details by user email id");
        Optional<Cart> existingCart = cartRepository.findByUserEmail(userEmail);

        if (existingCart.isPresent()) {
            Cart cart = existingCart.get();
            Map<String, Integer> map = cart.getProductQuantity();
            Map<String, Product> products = cart.getCartProducts();
            if (map.containsKey(productId)) {
                int count = map.get(productId);
                if (count > 1) {
                    map.put(productId, count - 1);
                } else {
                    map.remove(productId);
                    products.remove(productId);
                    cart.setCartProducts(products);
                }
                cart.setProductQuantity(map);
                CartDto c = updateCart(convertEntityToDto(cart));
                Cart finalCart = cartRepository.save(convertDtoToEntity(c));

                return sendCartToRabbitMQ(convertEntityToDto(finalCart));

            } else {
                log.error("product Not Exists in DB");
                throw new ProductNotExistInCartException(CartConstants.PRODUCT_NOT_FOUND_MESSAGE + productId);
            }
        } else {
            log.error("User Not Exists in DB");
            throw new UserNotFoundException(CartConstants.USER_NOT_FOUND_MESSAGE + userEmail);
        }

    }

    /*
     *
     * @Description : This method is used to clear cart
     * @param : Takes userId as parameter
     * @returns : Returns CartDto entity
     * @throws :It throws User Not Found Exception if user is not available with user email
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    @Override
    public CartDto clearCart(String userEmail) {
        log.info("Getting Cart details by user email id");
        Optional<Cart> existingCart = cartRepository.findByUserEmail(userEmail);
        if(existingCart.isPresent()) {
            Cart cart = existingCart.get();
            cart.setCartProducts(new HashMap<>());
            cart.setProductQuantity(new HashMap<>());
            cart.setTotalAmount(0);
            cart.setTotalItems(0);
            CartDto cartDto = convertEntityToDto(cartRepository.save(cart));
            log.info("Cart Cleared successfully");
            return sendCartToRabbitMQ(cartDto);
        }else {
            log.error("User Not Exists in DB");
            throw new UserNotFoundException(CartConstants.USER_NOT_FOUND_MESSAGE + userEmail);
        }
    }

    /*
     *
     * @Description : This method is used to get all Cart Details
     * @param :
     * @returns : Returns List of carts
     * @throws :CartNotFoundException if no cart exists in DB
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    @Override
    public List<Cart> getAllCart() {
        if (cartRepository.findAll().isEmpty()) {
            log.error("No Cart Exists in DB");
            throw new CartNotFoundException(CartConstants.CARTS_NOT_EXIST_MESSAGE);
        }
        log.info("All Cart details got successfully");
        log.debug("Inside CartServiceImpl -- getAllCart method");
        return cartRepository.findAll();
    }

    /*
     *
     * @Description : this method is used to Convert Cart entity to Cart dto
     * @param : Takes cart as parameter
     * @returns : Returns CartDto
     * @throws :
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    public CartDto convertEntityToDto(Cart cart) {
        //log.info("Convert Entity to DTO ");
        ModelMapper modelMapper = new ModelMapper();

        CartDto cartDto = modelMapper.map(cart, CartDto.class);
        Map<String, Product> cartProductDtos = new HashMap<>();
        Map<String, Integer> productDtoQuantity = new HashMap<>();

        cartProductDtos.putAll(cart.getCartProducts());
        productDtoQuantity.putAll(cart.getProductQuantity());

        cartDto.setCartId(cart.getCartId());
        cartDto.setUserEmail(cart.getUserEmail());
        cartDto.setCartProducts(cartProductDtos);
        cartDto.setProductQuantity(productDtoQuantity);
        cartDto.setTotalAmount(cart.getTotalAmount());
        cartDto.setTotalItems(cart.getTotalItems());
        return cartDto;
    }

    /*
     *
     * @Description : this method is used to Convert Cart Dto to Cart entity
     * @param : Takes CartDto as parameter
     * @returns : Returns Cart entity
     * @throws :
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    public Cart convertDtoToEntity(CartDto cartDto) {
        //log.info("Convert DTO  to Entity ");
        ModelMapper modelMapper = new ModelMapper();

        Cart cart = modelMapper.map(cartDto, Cart.class);

        Map<String, Product> cartProducts = new HashMap<>();
        Map<String, Integer> productQuantity = new HashMap<>();

        cartProducts.putAll(cartDto.getCartProducts());
        productQuantity.putAll(cartDto.getProductQuantity());

        cart.setUserEmail(cartDto.getUserEmail());
        cart.setCartId(cartDto.getCartId());
        cart.setCartProducts(cartProducts);
        cart.setProductQuantity(productQuantity);
        cart.setTotalItems(cartDto.getTotalItems());
        cart.setTotalAmount(cartDto.getTotalAmount());

        return cart;
    }

    /*
     *
     * @Description : This method is used to send cart to RabbitMQ
     * @param : Takes CartDto as parameter
     * @returns : Returns Cart DTO entity
     * @throws :
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    public CartDto sendCartToRabbitMQ(CartDto cart) {
        template.convertAndSend(ProducerConfig.EXCHANGE, "", cart);
        log.info("Cart Send to Payment and Order Service");
        return cart;
    }

    /*
     *
     * @Description : This method is used to create cart for New users
     * @param : Takes user Email as parameter
     * @returns : It will Create Empty Cart in DB
     * @throws :
     * @Created by : Pravin Ghodake
     * @createdDate : 22nd November 2022
     *
     */
    public void createCartForNewUser(UserDto userDto) {
        Cart cart = new Cart();
        cart.setUserEmail(userDto.getUserEmail());
        cart.setAddress(userDto.getAddress());
        cart.setCartProducts(new HashMap<String, Product>());
        cart.setProductQuantity(new HashMap<String, Integer>());
        cart.setTotalItems(0);
        cart.setTotalAmount(0);
        log.debug("Inside CartServiceImpl -- createCartForNewUser method");
        log.info("Cart Created for New User successfully");
        cartRepository.save(cart);
    }
}
