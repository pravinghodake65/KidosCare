package com.stackroute.cartservice.util;

public class CartConstants {
    //public static final String DELETE_MESSAGE = "Product Removed Successfully";
    public static final String CARTS_NOT_EXIST_MESSAGE = "Cart details Not available to get";
    public static final String CART_NOT_FOUND_MESSAGE = "Cart details Not available for cart id :";
    public static final String USER_NOT_FOUND_MESSAGE = "User details not available for User email :";
    public static final String PRODUCT_NOT_FOUND_MESSAGE = "Product details Not available with product id :";

}
