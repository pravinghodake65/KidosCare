package com.stackroute.rabbitmq.domain;

import lombok.*;



@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CartRequestDto {
 /*   private List<ProductDTO> productDTOS;
    private List<UserDto> userDtos;*/

    private String productId;
    private String userId;

}
