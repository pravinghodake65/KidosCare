package com.stackroute.rabbitmq.domain;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class ProductDTO {
    @MongoId
    private String productId;

    private String productName;
    private String productCategory;
    private String productDescription;
    private String mfgDate;
    private String expDate;
   // private Integer productUnits;
    private Long mrpPrice;
    private Long bestPrice;

    private String productImage;
}
