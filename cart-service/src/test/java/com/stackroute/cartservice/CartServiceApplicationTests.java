package com.stackroute.cartservice;

import com.stackroute.cartservice.dto.CartDto;
import com.stackroute.cartservice.entity.Cart;
import com.stackroute.cartservice.entity.Product;
import com.stackroute.cartservice.repository.CartRepository;
import com.stackroute.cartservice.service.CartService;
import com.stackroute.rabbitmq.domain.AddressDto;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class CartServiceApplicationTests {

    @Autowired
    private CartService cartService;
    @MockBean
    private CartRepository cartRepository;

    /**
     * Tests get all Cart Details
     */
    @Test
    public void testGetAllCartDetails() {
        List<Cart> cartList = new ArrayList<>();
        Map<String, Product> cartProducts1 = new HashMap<>();
        Map<String, Product> cartProducts2 = new HashMap<>();

        Map<String, Integer> productQuantity1 = new HashMap<>();
        Map<String, Integer> productQuantity2 = new HashMap<>();

        AddressDto address1 = new AddressDto(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);
        AddressDto address2 = new AddressDto(2, 12, "Motipur complex", "High Court  road", "G.M Road", "vennala", "kochi", "kerala", 335647);

        cartProducts1.put("101", new Product("b96b8055-dbef-478e-a755-3d91b24150c5", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 1299L, 990L, "262d76a5-7266-421c-8c75-42f40ba3af51.png"));
        cartProducts1.put("101", new Product("b96b8055-dbef-478e-a755-3d91b24150c6", "Jeans", "Accessories", "Its  product made in India Varient ", "19/01/2021", "19/01/2024", 2199L, 1990L, "262d76a5-7266-421c-8c75-42f40ba3af51.png"));

        productQuantity1.put("101", 2);
        productQuantity2.put("102", 1);


        cartList.add(new Cart("1", "pravin.ghodake@gmail.com", cartProducts1, productQuantity1, 2, 1980f, address1));
        cartList.add(new Cart("2", "prasad.ghodake@gmail.com", cartProducts2, productQuantity2, 1, 1990f, address2));

        when(cartRepository.findAll()).thenReturn(cartList);
        assertEquals(2, cartService.getAllCart().size());
    }

    /**
     * Tests add product to cart
     */
    @Test
    public void testAddToCart() {
        Map<String, Product> cartProducts1 = new HashMap<>();
        cartProducts1.put("101", new Product("b96b8055-dbef-478e-a755-3d91b24150c5", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 1299L, 990L, "262d76a5-7266-421c-8c75-42f40ba3af51.png"));

        Map<String, Integer> productQuantity1 = new HashMap<>();
        productQuantity1.put("101", 2);

        AddressDto address1 = new AddressDto(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);

        Cart cart = new Cart("1", "pravin.ghodake@gmail.com", cartProducts1, productQuantity1, 2, 1980f, address1);
        CartDto cartDto = cartService.convertEntityToDto(cart);
        when(cartRepository.save(cart)).thenReturn(cart);
        assertEquals(cart.getUserEmail(), cartDto.getUserEmail());
    }


    /**
     * Tests get Cart Details by user  email Id
     */
    @Test
    public void testGetCartDetailsByEmailId() {
        String emailId = "pravin.ghodake@gmail.com";

        Map<String, Product> cartProducts1 = new HashMap<>();
        cartProducts1.put("101", new Product("b96b8055-dbef-478e-a755-3d91b24150c5", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 1299L, 990L, "262d76a5-7266-421c-8c75-42f40ba3af51.png"));

        Map<String, Integer> productQuantity1 = new HashMap<>();
        productQuantity1.put("101", 2);

        AddressDto address1 = new AddressDto(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);

        Cart cart = new Cart("1", "pravin.ghodake@gmail.com", cartProducts1, productQuantity1, 2, 1980f, address1);

        when(cartRepository.findByUserEmail(emailId)).thenReturn(Optional.of(cart));
        assertNotNull(cartService.getCartByUserId(emailId));

    }

    /**
     * Tests get Cart Details by Cart Id
     */
    @Test
    public void testGetCartDetailsByCartId() {
        String cartId = "1";

        Map<String, Product> cartProducts1 = new HashMap<>();
        cartProducts1.put("101", new Product("b96b8055-dbef-478e-a755-3d91b24150c5", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 1299L, 990L, "262d76a5-7266-421c-8c75-42f40ba3af51.png"));

        Map<String, Integer> productQuantity1 = new HashMap<>();
        productQuantity1.put("101", 2);

        AddressDto address1 = new AddressDto(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);

        Cart cart = new Cart("1", "pravin.ghodake@gmail.com", cartProducts1, productQuantity1, 2, 1980f, address1);

        when(cartRepository.findByCartId(cartId)).thenReturn(cart);
        assertNotNull(cartService.getCartByCartId(cartId));

    }

}
