package com.stackroute.emailservice.controller;
import com.stackroute.emailservice.dto.EmailRequest;
import com.stackroute.emailservice.dto.EmailResponse;
import com.stackroute.emailservice.dto.OrderEmail;
import com.stackroute.emailservice.service.EmailService;
import com.stackroute.emailservice.utils.AppConstants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/email")
public class EmailController {
    @Autowired
    private EmailService service;



    @PostMapping("/registration")
    @Operation(summary = "This Method will send email to the registered person after successfully Registration")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Registration Mail Send successfully !!!",
                    content = {
                            @Content(mediaType = "application/json",schema = @Schema(implementation = EmailResponse.class))
                    }),
            @ApiResponse(responseCode = "500",description = "Their is Some Internal issue.")
    })
    public ResponseEntity<EmailResponse> sendRegistrationEmail(@RequestBody EmailRequest request) {
        HttpStatus status = HttpStatus.OK;
        Map<String, Object> model = new HashMap<>();
        model.put("email", AppConstants.EMAIL);
        model.put("location", AppConstants.LOCATION);
        model.put("name", request.getName());
        EmailResponse response =service.sendRegistrationEmail(request, model);
        if(response.isStatus()==false){
            status=HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<EmailResponse>(response, status);

    }
    @PostMapping("/order")
    @Operation(summary = "This Method will send email to the Buyer after successfully Order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Order Mail Send successfully !!!",
                    content = {
                            @Content(mediaType = "application/json",schema = @Schema(implementation = EmailResponse.class))
                    }),
            @ApiResponse(responseCode = "500",description = "Their is Some Internal issue.")
    })

    ResponseEntity<EmailResponse> sendOrderEmail(@RequestBody OrderEmail orderEmail){
        HttpStatus status = HttpStatus.OK;
        Map<String,Object> model=new HashMap<>();
        model.put("firstName", orderEmail.getFirstName());
        model.put("productList", orderEmail.getProduct());
        model.put("orderId", orderEmail.getOrderId());
        model.put("orderValue", orderEmail.getOrderValue());
        EmailResponse response =service.orderEmail(orderEmail, model);
        if(response.isStatus()==false){
            status=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<EmailResponse>(response, status);

    }


}