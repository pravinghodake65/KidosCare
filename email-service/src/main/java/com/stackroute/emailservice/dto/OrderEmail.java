package com.stackroute.emailservice.dto;

import lombok.Data;

import java.util.List;

@Data
public class OrderEmail {
    private String emailId;
    private String orderId;
    private String firstName;
    private List<Product> product;
    private double orderValue;
}
