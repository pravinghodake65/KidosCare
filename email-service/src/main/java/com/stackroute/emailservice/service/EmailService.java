package com.stackroute.emailservice.service;



import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.stackroute.emailservice.dto.EmailRequest;

import com.stackroute.emailservice.dto.EmailResponse;

import com.stackroute.emailservice.dto.OrderEmail;
import com.stackroute.emailservice.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class EmailService implements IEmailService {

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration config;


    public EmailResponse sendRegistrationEmail(EmailRequest request, Map<String, Object> model)  {
        EmailResponse response = new EmailResponse();
        MimeMessage message = sender.createMimeMessage();
        try {

            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Template t = config.getTemplate("email-template.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(request.getTo());
            helper.setText(html, true);
            helper.setSubject(AppConstants.REGISTRATION_SUBJECT);

            sender.send(message);

            response.setMessage("mail send to : " + request.getTo());
            response.setStatus(Boolean.TRUE);

        } catch (MessagingException | IOException | TemplateException e) {
            response.setMessage("Mail Sending failure : "+e.getMessage());
            response.setStatus(Boolean.FALSE);
        }
        catch (Exception e) {
            response.setMessage("Mail Sending failure : "+e.getMessage());
            response.setStatus(Boolean.FALSE);
        }

        return response;
    }


    @Override
    public EmailResponse orderEmail(OrderEmail request, Map<String, Object> model) {

        EmailResponse response = new EmailResponse();
        MimeMessage message = sender.createMimeMessage();
        try {

            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());


            Template t = config.getTemplate("order-template.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(request.getEmailId());
            helper.setText(html, true);
            helper.setSubject(AppConstants.ORDER_SUBJECT);

            sender.send(message);

            response.setMessage("mail send to : " + request.getEmailId());
            response.setStatus(Boolean.TRUE);

        } catch (MessagingException | IOException | TemplateException e) {
            response.setMessage("Mail Sending failure : " + e.getMessage());
            response.setStatus(Boolean.FALSE);
        }

        return response;
    }


}