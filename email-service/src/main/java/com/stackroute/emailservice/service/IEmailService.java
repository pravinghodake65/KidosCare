package com.stackroute.emailservice.service;

import com.stackroute.emailservice.dto.EmailRequest;
import com.stackroute.emailservice.dto.EmailResponse;
import com.stackroute.emailservice.dto.OrderEmail;

import java.util.Map;

public interface IEmailService {
     EmailResponse sendRegistrationEmail(EmailRequest request, Map<String, Object> model);


    EmailResponse orderEmail(OrderEmail request, Map<String, Object> model);
}
