package com.stackroute;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.stackroute.emailservice.dto.EmailRequest;
import com.stackroute.emailservice.service.EmailService;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;



@RunWith(SpringRunner.class)
@ContextConfiguration

class EmailServiceApplicationTests {
@Autowired
	private EmailService emailServiceImpl;
/*
@Autowired
	private JavaMailSender javaMailSender;

	private MimeMessage mimeMessage;



	Map<String,String> model=new HashMap<>();
	@Before
	public void before() {
		mimeMessage = new MimeMessage((Session)null);
		javaMailSender = mock(JavaMailSender.class);
		when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
		emailServiceImpl = new EmailService();
	}
*/

/*
	@Test
	public void emailTest() throws MessagingException {
		Map<String,Object> model=new HashMap<>();
		model.put("email", "Connect2kidoscare@gmail.com");
		model.put("location", "Bangalore,India");
		model.put("name", "rashmi");
		String recipient = "butu.rashmi@gmail.com";
		EmailRequest request = new EmailRequest();
		request.setTo(recipient);
		request.setName("Rashmi");
		emailServiceImpl.sendEmail(request,model);
		assertEquals(recipient, mimeMessage.getRecipients(Message.RecipientType.TO)[0].toString());
	}
*/


	/*@Test
	public void sendSimpleEmailWithCC() {
		// Runs a Dumbster simple SMTP server - default config
		SimpleSmtpServer server = SimpleSmtpServer.start();
		String from = "whoever@from.com";
		String to = "whoever@to.com";
		String messageText = "Good message";
		String title = "Test message";
		String cc = "whoever@cc.com";
		Assert.assertTrue(mailSender
				.sendEmail(from, to, cc, title, messageText));
		server.stop();*/


	@Resource
	private JavaMailSenderImpl emailSender;

	private GreenMail testSmtp;

	@Before
	public void testSmtpInit(){
		testSmtp = new GreenMail(ServerSetupTest.SMTP);
		testSmtp.start();

		//don't forget to set the test port!
		emailSender.setPort(3025);
		emailSender.setHost("localhost");
	}

	@Test
	public void testEmail() throws InterruptedException, MessagingException {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom("test@sender.com");
		message.setTo("test@receiver.com");
		message.setSubject("test subject");
		message.setText("test message");
		emailSender.send(message);

		Message[] messages = testSmtp.getReceivedMessages();
		assertEquals(1, messages.length);
		assertEquals("test subject", messages[0].getSubject());
		String body = GreenMailUtil.getBody(messages[0]).replaceAll("=\r?\n", "");
		assertEquals("test message", body);
	}

	@After
	public void cleanup(){
		testSmtp.stop();
	}

}
