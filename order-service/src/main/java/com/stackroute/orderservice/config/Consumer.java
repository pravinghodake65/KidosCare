package com.stackroute.orderservice.config;

import com.stackroute.orderservice.entity.Cart;
import com.stackroute.orderservice.exception.ProfileAlreadyExit;
import com.stackroute.orderservice.service.CartServiceImpl;

import com.stackroute.rabbitmq.domain.CartDto;
import com.stackroute.rabbitmq.domain.PaymentDto;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    @Autowired
    CartServiceImpl cartService;

    public Consumer(CartServiceImpl cartService) {
        this.cartService = cartService;
    }
    @RabbitListener(queues= MessageConfiguration.order_queue)
    public void getProductDtoFromRabbitMq(CartDto cartDto)  {
        cartService.saveCartData(cartDto);
        System.out.println("Order is getting data successfully and the ordered products details are "+cartDto);
    }

    @RabbitListener(queues= MessageConfiguration.paymentQueue)
   public void getPaymentDtoFromRabbitMq(PaymentDto  paymentDto)  {
        cartService.savePaymentData(paymentDto);
        System.out.println("Got payment details  successfully");
    }

}



