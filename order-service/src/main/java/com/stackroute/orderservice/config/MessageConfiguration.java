package com.stackroute.orderservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfiguration {
    public static final String exchangeName = "exchange";
    public static final String paymentQueue ="producer_payment_queue";
public static final String payment_routing = "payment_routing";
    public static final String order_queue="order_queue";

    public static final String order_routing = "order_routing";

    @Bean
   public Queue paymentQueue() {
        return new Queue(paymentQueue, false);
    }

     @Bean
    public DirectExchange exchange() {
        return new DirectExchange(exchangeName);
    }

    @Bean
    public Binding bindingPayment(Queue paymentQueue, DirectExchange exchange) {
        return BindingBuilder.bind(paymentQueue()).to(exchange).with(payment_routing);
    }
@Bean
public Queue productQueue() {
    return new Queue(order_queue, false);
}

    @Bean
    public Binding productBinding(Queue productQueue, DirectExchange exchange) {
        return BindingBuilder.bind(productQueue()).to(exchange()).with(order_routing);
    }
    @Bean
    public MessageConverter converter()
    {
        return new  Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }


}




