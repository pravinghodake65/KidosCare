package com.stackroute.orderservice.controller;

import com.stackroute.orderservice.dto.OrderDto;
import com.stackroute.orderservice.entity.Order;
import com.stackroute.orderservice.exception.IdNotFound;
import com.stackroute.orderservice.exception.ProfileAlreadyExit;
import com.stackroute.orderservice.service.OrderService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    Log log = LogFactory.getLog(OrderController.class);

    @Autowired
        private OrderService orderService;

    @GetMapping("/order")
    public ResponseEntity<?> getAllOrders() {
        return new ResponseEntity<>(orderService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/findByOrderId/{orderId}")
    public ResponseEntity<Order> getDetailsByOrderId(@PathVariable String orderId) {
        Order order = orderService.getByOrderId(orderId);
            return new ResponseEntity<>(order, HttpStatus.OK);
        }

    @GetMapping("/findByEmailId/{emailId}")
    public ResponseEntity<Order> getDetailsByEmailId(@PathVariable String emailId) {
        Order order = orderService.getByEmailId(emailId);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping("/UpdateOrderStatus/{orderId}")
    public ResponseEntity <Order> UpdateOrderStatus(@PathVariable String orderId, @RequestParam String status) throws IdNotFound {
        return  ResponseEntity.ok().body(this.orderService.UpdateOrderStatus(orderId,status));
    }

}



