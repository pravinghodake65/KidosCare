package com.stackroute.orderservice.dto;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class AddressDto {

    @MongoId
    private long doorNo;
    private String streetName;
    private String landMark;
    private String city;
    private String state;
    private long pincode;

    public AddressDto(int i, String complex, String gandhiRoad, String gandhiMaidan, String stadium, String hyd, String ts, int i1) {
    }
}
