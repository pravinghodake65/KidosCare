package com.stackroute.orderservice.dto;

import com.stackroute.orderservice.entity.Status;
import com.stackroute.rabbitmq.domain.CartDto;
import com.stackroute.rabbitmq.domain.PaymentDto;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Map;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class OrderDto {

    @MongoId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id= UUID.randomUUID().toString();
    private String orderId;
    private String emailId;
    private int quantity;
    private AddressDto addressDto;
    private CartDto cartDto;
    private PaymentDto paymentDto;
    private String orderDate;
    private Status orderStatus;
    private String deliveryDate;


}
