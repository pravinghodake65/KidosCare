package com.stackroute.orderservice.entity;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Address {
    @MongoId
    private long doorNo;
    private String streetName;
    private String landMark;
    private String city;
    private String state;
    private long pincode;

}
