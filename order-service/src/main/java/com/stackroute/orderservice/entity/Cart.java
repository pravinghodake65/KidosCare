package com.stackroute.orderservice.entity;


import com.stackroute.orderservice.dto.AddressDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Map;
import java.util.UUID;

@Document(collection = "cart")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    @MongoId
    private String cartId = UUID.randomUUID().toString();
    private String userEmail;
    private Map<String, Product> cartProducts;
    private Map<String, Integer> productQuantity;
    private AddressDto address;

}
