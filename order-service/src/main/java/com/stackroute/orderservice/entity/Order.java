package com.stackroute.orderservice.entity;

import com.stackroute.orderservice.dto.AddressDto;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Document(collection = "Order")
public class Order {
    @MongoId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id= UUID.randomUUID().toString();
    private String emailId;
    private String orderId;
    private LocalDate orderDate;
    private Map<String, Product> cartProducts;
    private Map<String, Integer> productQuantity;
    private int totalItems;
    private String paymentId;
    private String paymentStatus;
    private Status orderStatus;
    private float totalAmount;
    private AddressDto address;

    public void getTotalAmount(float v) {
    }
}
