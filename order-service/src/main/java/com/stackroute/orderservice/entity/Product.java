package com.stackroute.orderservice.entity;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.UUID;

@Document(collection = "Product")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Product {
    @MongoId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String productId = UUID.randomUUID().toString();
    private String productName;
    private String productCategory;
    private String productDescription;
    private String mfgDate;
    private String expDate;
    private Integer productUnits;
    private Long mrpPrice;
    private Long bestPrice;
    private String productImage;
}
