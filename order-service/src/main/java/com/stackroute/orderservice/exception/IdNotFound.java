package com.stackroute.orderservice.exception;

public class IdNotFound extends  Exception {

    public IdNotFound(String message) {
        super(message);
    }

    public IdNotFound(){

    }
}
