package com.stackroute.orderservice.exception;

public class ProfileAlreadyExit extends Exception {

    public ProfileAlreadyExit(String message) {
        super(message);
    }

    public ProfileAlreadyExit() {

    }
}
