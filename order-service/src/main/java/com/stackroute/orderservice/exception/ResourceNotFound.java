package com.stackroute.orderservice.exception;

public class ResourceNotFound extends Exception{

    public ResourceNotFound(String message) {
        super(message);
    }
    public ResourceNotFound(){

    }
}
