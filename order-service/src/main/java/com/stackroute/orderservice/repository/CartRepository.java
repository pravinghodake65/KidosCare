package com.stackroute.orderservice.repository;

import com.stackroute.rabbitmq.domain.CartDto;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CartRepository extends MongoRepository<CartDto, String> {

    Optional<CartDto> findByUserEmail(String email);
}
