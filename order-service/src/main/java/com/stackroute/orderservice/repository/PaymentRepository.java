package com.stackroute.orderservice.repository;

import com.stackroute.rabbitmq.domain.PaymentDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends MongoRepository<PaymentDto, String> {
}
