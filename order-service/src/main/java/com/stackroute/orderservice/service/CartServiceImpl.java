package com.stackroute.orderservice.service;

import com.stackroute.orderservice.entity.Order;
import com.stackroute.orderservice.repository.CartRepository;
import com.stackroute.orderservice.repository.PaymentRepository;
import com.stackroute.rabbitmq.domain.CartDto;
import com.stackroute.rabbitmq.domain.PaymentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartServiceImpl {
    @Autowired
    public CartRepository cartRepository;

    @Autowired
    public PaymentRepository paymentRepository;

    @Autowired
    public OrderServiceImpl orderService;

    public CartServiceImpl(CartRepository cartRepository, PaymentRepository paymentRepository) {
        this.cartRepository = cartRepository;
        this.paymentRepository = paymentRepository;
    }
    public void saveCartData(CartDto cartDto){
        Optional<CartDto> cart = cartRepository.findByUserEmail(cartDto.getUserEmail());
        if(cart.isPresent()){
            CartDto cartDetails = cart.get();
            cartDetails.setCartProducts(cartDto.getCartProducts());
            cartDetails.setAddressDto(cartDto.getAddressDto());
            cartDetails.setTotalItems(cartDto.getTotalItems());
            cartDetails.setProductQuantity(cartDto.getProductQuantity());
            cartDetails.setTotalAmount(cartDto.getTotalAmount());
            cartRepository.save(cartDetails);
        }
        else{
            CartDto newCartDto = new CartDto();
            newCartDto.setCartId(cartDto.getCartId());
            newCartDto.setCartProducts(cartDto.getCartProducts());
            newCartDto.setUserEmail(cartDto.getUserEmail());
            newCartDto.setAddressDto(cartDto.getAddressDto());
            newCartDto.setProductQuantity(cartDto.getProductQuantity());
            newCartDto.setTotalItems(cartDto.getTotalItems());
            newCartDto.setTotalAmount(cartDto.getTotalAmount());
            cartRepository.save(newCartDto);
        }
    }

    public void savePaymentData(PaymentDto paymentDto){
//        paymentRepository.save(paymentDto);
        orderService.createOrder(paymentDto);
    }
}
