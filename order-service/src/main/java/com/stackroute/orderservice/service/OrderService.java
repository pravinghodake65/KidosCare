package com.stackroute.orderservice.service;

import com.stackroute.orderservice.dto.OrderDto;
import com.stackroute.orderservice.exception.IdNotFound;
import com.stackroute.orderservice.exception.ProfileAlreadyExit;
import com.stackroute.orderservice.entity.Order;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface OrderService {

    List<Order> findAll();

//    OrderDto createOrder(OrderDto orderDto) throws ProfileAlreadyExit;

    Order getByOrderId(String orderId);

    Order UpdateOrderStatus(String orderId, String status) throws IdNotFound;

    /*
     *convert dto to entity
     */
    Order convertToEntity(OrderDto orderDto);

    /*
     *convert entity to dto
     */
    OrderDto convertToDto(Order order);

    Order getByEmailId(String emailId);
}
