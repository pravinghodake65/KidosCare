package com.stackroute.orderservice.service;
import com.stackroute.orderservice.dto.AddressDto;
import com.stackroute.orderservice.dto.OrderDto;
import com.stackroute.orderservice.entity.*;
import com.stackroute.orderservice.exception.IdNotFound;

import com.stackroute.orderservice.repository.CartRepository;
import com.stackroute.orderservice.repository.OrderRepository;
import com.stackroute.orderservice.repository.PaymentRepository;
import com.stackroute.orderservice.utils.AppConstants;

import com.stackroute.rabbitmq.domain.CartDto;
import com.stackroute.rabbitmq.domain.PaymentDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    Log log = LogFactory.getLog(OrderServiceImpl.class);

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CartRepository cartRepository;
    @Autowired
    PaymentRepository paymentRepository;
    public OrderServiceImpl(OrderRepository orderRepository, CartRepository cartRepository, PaymentRepository paymentRepository) {
        this.orderRepository = orderRepository;
        this.cartRepository = cartRepository;
        this.paymentRepository = paymentRepository;
    }

    @Override
    public List<Order> findAll() {
        log.info("find All Orders from Database");
       List<Order> orderList = orderRepository.findAll();
        return orderList;
    }

    @Override
    public Order getByOrderId(String orderId) {
        log.info("Getting Order details by orderId " );
        Order order = orderRepository.findByOrderId(orderId).get();
        return  order;
    }

    @Override
    public Order UpdateOrderStatus(String orderId, String status) throws IdNotFound {
        log.info(" Update  Order details by orderId ");
        Optional<Order> order = orderRepository.findByOrderId(orderId);

       if (order.isPresent())
       {
       if (status.equalsIgnoreCase(String.valueOf(Status.Cancelled))){
            order.get().setOrderStatus(Status.Cancelled);
           return orderRepository.save (order.get());

       }
        else if(status.equalsIgnoreCase(String.valueOf(Status.Returns))){
            order.get().setOrderStatus(Status.Returns);
           return orderRepository.save (order.get());

       }else if(status.equalsIgnoreCase(String.valueOf(Status.Refund))) {
           order.get().setOrderStatus(Status.Refund);
           return orderRepository.save (order.get());

       }
        else
            throw new IdNotFound(AppConstants.NOT_FOUND_MESSAGE);
       }
       return order.get();
   }

    //convert dto to entity

    @Override
    public Order convertToEntity(OrderDto orderDto) {

        log.info("Convert DTO  to Entity ");
        Payment payment = new ModelMapper().map(orderDto.getPaymentDto(), Payment.class);
        Cart cart = new ModelMapper().map(orderDto.getCartDto(), Cart.class);
        Order order = new ModelMapper().map(orderDto, Order.class);

        return order;
    }

    //convert entity to dto

    @Override
    public OrderDto convertToDto(Order order) {

         log.info("Convert Entity to Dto");
        OrderDto orderDto = new ModelMapper().map(order, OrderDto.class);
        return  orderDto;
    }

    @Override
    public Order getByEmailId(String emailId) {
        log.info("Getting Order details by orderId " );
        Order order = orderRepository.findByEmailId(emailId).get();
        return  order;
    }

    public void createOrder(PaymentDto paymentDto){
        String emailId = paymentDto.getUserEmail();
        if(cartRepository.findByUserEmail(emailId).isPresent()){
            CartDto cart = cartRepository.findByUserEmail(emailId).get();
            Order order = new Order();
            order.setOrderDate(LocalDate.now());
            order.setOrderStatus(Status.Confirm);

            log.info("Getting data from payment service");
            order.setEmailId(paymentDto.getUserEmail());
            order.setOrderId(paymentDto.getOrderId());
            order.setTotalAmount(paymentDto.getAmount());
            order.setPaymentStatus(paymentDto.getStatus());
            order.setPaymentId(paymentDto.getPayment_id());

            log.info("Getting data from cart service");
            order.setTotalItems(cart.getTotalItems());
            order.setAddress(cart.getAddressDto());
            order.setCartProducts(cart.getCartProducts());
            order.setProductQuantity(cart.getProductQuantity());

            orderRepository.save(order);
        }
    }
    }

