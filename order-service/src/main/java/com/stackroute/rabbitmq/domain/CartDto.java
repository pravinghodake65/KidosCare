package com.stackroute.rabbitmq.domain;

import com.stackroute.orderservice.dto.AddressDto;
import com.stackroute.orderservice.entity.Product;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.MongoId;


import java.util.Map;
import java.util.UUID;

@Data
public class CartDto {

    @MongoId
    private String cartId ;
    private String userEmail;
    private Map<String, Product> cartProducts;
    private Map<String, Integer> productQuantity;
    private AddressDto addressDto;
    private int totalItems;
    private float totalAmount;
}
