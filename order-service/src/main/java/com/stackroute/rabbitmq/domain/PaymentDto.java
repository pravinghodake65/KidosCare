package com.stackroute.rabbitmq.domain;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PaymentDto {
  private String userEmail ;
  private float amount;
  private String orderId;
  private String payment_id;
  private String status;
}


