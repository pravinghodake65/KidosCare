package com.stackroute.rabbitmq.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProductDto {
    private String productId ;
    private String productName;
    private String productCategory;
    private String productDescription;
    private String mfgDate;
    private String expDate;
    private Long mrpPrice;
    private Long bestPrice;
    private String productImage;
}
