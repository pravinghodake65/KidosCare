package com.stackroute.orderservice;

import com.stackroute.orderservice.dto.AddressDto;
import com.stackroute.orderservice.entity.Order;
import com.stackroute.orderservice.entity.Product;
import com.stackroute.orderservice.entity.Status;
import com.stackroute.orderservice.exception.IdNotFound;
import com.stackroute.orderservice.repository.OrderRepository;
import com.stackroute.orderservice.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
@RunWith(SpringRunner.class)
@SpringBootTest
class OrderServiceApplicationTests {
	@Autowired
	private OrderService orderService;

	@MockBean
	private OrderRepository orderRepository;

	/**
	 * Tests get all User Details
	 */
	@Test
	void testgetAllOrders() {

		List<Order> orders = new ArrayList<Order>();

		Map<String, Product> cartProducts = new HashMap<>();
		cartProducts.put("101", new Product("101", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 128, 990L, 400L, "262d76a5-7266-421c-8c75-42f40ba3af50.png"));

		Map<String, Integer> productQuantity = new HashMap<>();
		productQuantity.put("101", 2);

		AddressDto address= new AddressDto( 10, "complex", "gandhi road", "gandhi maidan", "stadium", "hyd", "ts", 500072);

		Order order = new Order();
		order.setId("u1");
		order.setEmailId("1");
		order.setOrderId("description");
		order.setOrderDate(LocalDate.now());
		order.setCartProducts(cartProducts);
		order.setProductQuantity(productQuantity);
		order.setTotalItems(12);
		order.setPaymentId("p1");
		order.setPaymentStatus("paid");
		order.setOrderStatus(Status.Returns);
		order.getTotalAmount(100f);
		order.setAddress(address);

		orders.add(order);

		when(orderRepository.findAll()).thenReturn(orders);

		assertEquals(1, orderService.findAll().size());
	}


	/**
	 * Tests get Order Details by user  email Id
	 */
	@Test
	public void testGetOrderDetailsByEmailId() {

		String emailId = "kanala.anjaneyulu@gmail.com";

		Map<String, Product> cartProducts = new HashMap<>();
		cartProducts.put("101", new Product("101", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 128, 990L, 400L, "262d76a5-7266-421c-8c75-42f40ba3af50.png"));

		Map<String, Integer> productQuantity = new HashMap<>();
		productQuantity.put("101", 2);

		AddressDto address = new AddressDto(10, "complex", "gandhi road", "gandhi maidan", "stadium", "hyd", "ts", 500072);

		Order order = new Order();
		order.setId("u1");
		order.setEmailId("1");
		order.setOrderId("description");
		order.setOrderDate(LocalDate.now());
		order.setCartProducts(cartProducts);
		order.setProductQuantity(productQuantity);
		order.setTotalItems(12);
		order.setPaymentId("p1");
		order.setPaymentStatus("paid");
		order.setOrderStatus(Status.Returns);
		order.getTotalAmount(100f);
		order.setAddress(address);

		when(orderRepository.findByEmailId(emailId)).thenReturn(Optional.of(order));
		assertNotNull(orderService.getByEmailId(emailId));
	}

	/**
	 * Tests get Order Details by Order Id
	 */
	@Test
	public void testGetOrderDetailsByOrderId() {
		String orderid = "1";

		Map<String, Product> cartProducts = new HashMap<>();
		cartProducts.put("101", new Product("101", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 128, 990L, 400L, "262d76a5-7266-421c-8c75-42f40ba3af50.png"));

		Map<String, Integer> productQuantity = new HashMap<>();
		productQuantity.put("101", 2);

		AddressDto address = new AddressDto(10, "complex", "gandhi road", "gandhi maidan", "stadium", "hyd", "ts", 500072);

		Order order = new Order();
		order.setId("u1");
		order.setEmailId("1");
		order.setOrderId("description");
		order.setOrderDate(LocalDate.now());
		order.setCartProducts(cartProducts);
		order.setProductQuantity(productQuantity);
		order.setTotalItems(12);
		order.setPaymentId("p1");
		order.setPaymentStatus("paid");
		order.setOrderStatus(Status.Returns);
		order.getTotalAmount(100f);
		order.setAddress(address);

		when(orderRepository.findByOrderId(orderid)).thenReturn(Optional.of(order));
		assertNotNull(orderService.getByOrderId(orderid));
	}

	/**
	 * Tests get Update Order status
	 */
	@Test
	public void testGetUpdateOrderStatus () throws IdNotFound {
		String orderId = "1";

		Map<String, Product> cartProducts = new HashMap<>();
		cartProducts.put("101", new Product("101", "Tshirt", "Accessories", "Its  product made in India Varient ", "09/12/2022", "09/12/2024", 128, 990L, 400L, "262d76a5-7266-421c-8c75-42f40ba3af50.png"));

		Map<String, Integer> productQuantity = new HashMap<>();
		productQuantity.put("101", 2);

		AddressDto address = new AddressDto(10, "complex", "gandhi road", "gandhi maidan", "stadium", "hyd", "ts", 500072);

		Order order = new Order();
		order.setId("u1");
		order.setEmailId("1");
		order.setOrderId("description");
		order.setOrderDate(LocalDate.now());
		order.setCartProducts(cartProducts);
		order.setProductQuantity(productQuantity);
		order.setTotalItems(12);
		order.setPaymentId("p1");
		order.setPaymentStatus("paid");
		order.setOrderStatus(Status.Returns);
		order.getTotalAmount(100f);
		order.setAddress(address);

		when(orderRepository.findByOrderId(orderId)).thenReturn(Optional.of(order));
		when(orderRepository.save(order)).thenReturn(order);
		assertNotNull(orderService.UpdateOrderStatus(orderId, "Returns"));
	}

}