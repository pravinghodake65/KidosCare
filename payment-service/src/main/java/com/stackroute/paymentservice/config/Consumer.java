package com.stackroute.paymentservice.config;

import com.stackroute.paymentservice.controller.PaymentController;
import com.stackroute.paymentservice.dto.CartDto;


import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



//
    @Component
    public class Consumer {
        @Autowired
        private PaymentController userService;





        @RabbitListener(queues="payment_queue")
        public void getUserDtoFromRabbitMq(CartDto cart)
        {

        userService.save(cart);
            System.out.println("consumed");
        }
    }

