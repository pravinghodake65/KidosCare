package com.stackroute.paymentservice.config;

import com.stackroute.paymentservice.entity.Payment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    Log log = LogFactory.getLog(Producer.class);
    private RabbitTemplate rabbitTemplate;
    private DirectExchange exchange ;


    @Autowired
    public Producer(RabbitTemplate rabbitTemplate, DirectExchange exchange) {
        super();
        this.rabbitTemplate = rabbitTemplate;
        this.exchange = exchange;

    }



    public Payment sendMessageToRabbitMq(Payment payment)
    {
        rabbitTemplate.convertAndSend(ProducerConfig.exchangeName, ProducerConfig.paymentRouting,payment);
        log.info("Data send from payment");
        return payment;
    }



}

