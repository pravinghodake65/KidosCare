package com.stackroute.paymentservice.config;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

    @Configuration
    public class ProducerConfig {

        public static final String exchangeName ="payment_exchange";
        public static final String paymentQueue ="producer_payment_queue";
        public static final String paymentRouting ="payment_routing";
        @Bean
        public Queue paymentQueue()
        {
            return new Queue(paymentQueue,false,false,false);
        }
        @Bean
        public DirectExchange directExchange()
        {
            return new DirectExchange(exchangeName);
        }
        @Bean
        public Jackson2JsonMessageConverter jsonMessageConverter()
        {
            return new Jackson2JsonMessageConverter();
        }
        @Bean
        Binding bindingUser(Queue paymentQueue, DirectExchange exchange)
        {
            return BindingBuilder.bind(paymentQueue).to(exchange).with(paymentRouting);

        }
        public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory)
        {
            final RabbitTemplate rabbitTemp=new RabbitTemplate(connectionFactory);
            rabbitTemp.setMessageConverter(jsonMessageConverter());
            return rabbitTemp;
        }

    }


