package com.stackroute.paymentservice.controller;

import java.util.Map;

import com.razorpay.RazorpayException;
import com.stackroute.paymentservice.dto.CartDto;
import com.stackroute.paymentservice.dto.PaymentDto;
import com.stackroute.paymentservice.entity.Cart;
import com.stackroute.paymentservice.entity.Payment;
import com.stackroute.paymentservice.service.PaymentService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.razorpay.Order;
import com.razorpay.RazorpayClient;



@Controller
@RequestMapping("/api/payment")
public class PaymentController {

   @Autowired
  private PaymentService paymentService;


    @RequestMapping("/start")
    public String dashboard()
    {
        return "payment_page";
    }

    //creating order for payment

    float amt;
    String cart_id;

    String userEmail;

    public  void save(CartDto cart){
        amt = cart.getTotalAmount();
        cart_id=cart.getCartId();
        userEmail= cart.getUserEmail();
    }
    @PostMapping("/create_order")
    @ResponseBody
    public String createOrder() throws RazorpayException
    {

        //System.out.println(cart);

      //amt=246;//we will get this from cart

        RazorpayClient client=new RazorpayClient("rzp_test_UGCJz7ry8QqYWd", "oRsd563Cqtl7ASVpUoVmtEQs");

        JSONObject ob=new JSONObject();
        ob.put("amount", amt*100);
        ob.put("currency", "INR");
        ob.put("receipt", "txn_235425");

        //creating new order

        Order order = client.orders.create(ob);
      //  System.out.println(order);


        PaymentDto paymentDto = new PaymentDto();

        paymentDto.setCart_id(cart_id);
        paymentDto.setOrderId(order.get("id"));
        paymentDto.setAmount(order.get("amount"));
        paymentDto.setStatus(order.get("status"));
        paymentDto.setPayment_id(null);
        paymentDto.setUserEmail(userEmail);
       paymentService.saveDetails(paymentDto);
        // paymentRepository.save(serviceImpl.convertToEntity(paymentDto)); //will save this to  data base.


        return order.toString();
    }

    @PostMapping("/update_order")
    public ResponseEntity<Payment> updateOrder(@RequestBody Map<String, Object> data) {




        return new ResponseEntity<  Payment>(  paymentService.updatePayment(data),HttpStatus.CREATED);
    }



}