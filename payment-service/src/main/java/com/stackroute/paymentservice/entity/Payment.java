package com.stackroute.paymentservice.entity;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Payment {

    @Id
    private String cart_id;
    private int amount;
    private String userEmail;
    private String orderId;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    private String payment_id;



    private String status;

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getPayment_id() {
        return payment_id;
    }
    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "cart_id='" + cart_id + '\'' +
                ", amount=" + amount +
                ", userEmail='" + userEmail + '\'' +
                ", orderId='" + orderId + '\'' +
                ", payment_id='" + payment_id + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

}
