package com.stackroute.paymentservice.service;


import com.stackroute.paymentservice.dto.PaymentDto;
import com.stackroute.paymentservice.entity.Payment;

import java.util.Map;




public interface PaymentService {
	//public Payment convertToEntity(PaymentDto paymentDto);
       Payment updatePayment(Map<String, Object> data);

	  void saveDetails(PaymentDto paymentDto);

//	public String createOrder() throws RazorpayException;
}
