package com.stackroute.paymentservice.service;

import java.util.Map;

import com.stackroute.paymentservice.config.Producer;
import com.stackroute.paymentservice.dto.PaymentDto;
import com.stackroute.paymentservice.entity.Payment;
import com.stackroute.paymentservice.repository.PaymentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class PaymentServiceImpl implements PaymentService{
	@Autowired
	private Producer producer;
	@Autowired
	PaymentRepository paymentRepository;
	PaymentDto paymentDetails=new PaymentDto();
	@Override
	public void saveDetails(PaymentDto paymentDto)
	{
		paymentRepository.save(convertToEntity(paymentDto));
		
		
	}

//	@Override
//	public String createOrder() throws RazorpayException {
//
//		int amt=246;//we will get this from cart
//
//		RazorpayClient client=new RazorpayClient("rzp_test_UGCJz7ry8QqYWd", "oRsd563Cqtl7ASVpUoVmtEQs");
//
//		JSONObject ob=new JSONObject();
//		ob.put("amount", amt*100);
//		ob.put("currency", "INR");
//		ob.put("receipt", "txn_235425");
//
//		//creating new order
//
//		Order order = client.orders.create(ob);
//		System.out.println(order);
//
//
//		PaymentDto paymentDto = new PaymentDto();
//
//
//		paymentDto.setOrderId(order.get("id"));
//		paymentDto.setAmount(order.get("amount"));
//		paymentDto.setStatus(order.get("status"));
//		paymentDto.setPayment_id(null);
//		paymentRepository.save(convertToEntity(paymentDto));
//		//paymentService.saveDetails(paymentDto);
//		// paymentRepository.save(serviceImpl.convertToEntity(paymentDto)); //will save this to  data base.
//
//
//		return order.toString();
//	}

	private Payment convertToEntity(PaymentDto paymentDto) {
       Payment payment =new Payment();
		
		payment.setCart_id(paymentDto.getCart_id());
		payment.setAmount(paymentDto.getAmount());
		payment.setOrderId(paymentDto.getOrderId());
		payment.setPayment_id(paymentDto.getPayment_id());
		payment.setStatus(paymentDto.getStatus());
		payment.setUserEmail(paymentDto.getUserEmail());
		return payment;
	}
	 PaymentDto convertToDto(Payment payment) {
	       PaymentDto paymentDto =new PaymentDto();
			
			paymentDto.setCart_id(payment.getCart_id());
			paymentDto.setAmount(payment.getAmount());
			paymentDto.setOrderId(payment.getOrderId());
			paymentDto.setPayment_id(payment.getPayment_id());
			paymentDto.setStatus(payment.getStatus());
		 paymentDto.setUserEmail(payment.getUserEmail());
//		Producer producer = new Producer();

			return paymentDto;
		}

	@Override
	public Payment updatePayment(Map<String, Object> data) {

		Payment updateOrder = paymentRepository.findByOrderId(data.get("order_id").toString());
		//PaymentDto paymentDto=convertToDto(updateOrder);


		updateOrder.setPayment_id(data.get("payment_id").toString());
		updateOrder.setStatus(data.get("status").toString());
		 paymentRepository.save(updateOrder);
		//System.out.println(payment);
		Payment updateOrder1 = paymentRepository.findByOrderId(data.get("order_id").toString());
		System.out.println(updateOrder1);
		return producer.sendMessageToRabbitMq(updateOrder1);
	}


}
