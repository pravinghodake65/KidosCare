package com.stackroute.productservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfiguration {
    public static final String product_queue = "product_queue";

    public static final  String EXCHANGE="exchange";
    public static final String PRODUCT_ROUTING_KEY = "routing_key";

    @Bean
    public Queue productQueue() {
        return new Queue(product_queue, false);
    }
    @Bean
    public TopicExchange topicExchange()
    {
        return new TopicExchange(EXCHANGE);
    }
    @Bean
    public Binding productBinding(Queue productQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(productQueue()).to(topicExchange()).with(PRODUCT_ROUTING_KEY);
    }
    @Bean
    public Jackson2JsonMessageConverter converter()
    {
        return new  Jackson2JsonMessageConverter();
    }
    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory)
    {
        RabbitTemplate rabbitTemp=new RabbitTemplate(connectionFactory);
        rabbitTemp.setMessageConverter(converter());
        return rabbitTemp;
    }
}
