package com.stackroute.productservice.config;

import com.stackroute.productservice.entity.Product;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.core.DirectExchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class Producer {


        private RabbitTemplate rabbitTemplate;
    TopicExchange topicExchange;

        @Autowired
        public Producer(RabbitTemplate rabbitTemplate, TopicExchange topicExchange) {
            super();
            this.rabbitTemplate = rabbitTemplate;
            this.topicExchange = topicExchange;
        }

    public void sendMessageToRabbitMq(Map<String, Product> cartDto)
    {
        rabbitTemplate.convertAndSend(MessageConfiguration.EXCHANGE,MessageConfiguration.PRODUCT_ROUTING_KEY,cartDto);
        System.out.println("Send product details to cart");
    }
}
