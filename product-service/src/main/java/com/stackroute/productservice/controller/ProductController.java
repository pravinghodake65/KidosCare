/*
 * @Author Karan Kumar
 */

package com.stackroute.productservice.controller;


import java.util.List;
import com.stackroute.productservice.dto.ProductDto;
import com.stackroute.productservice.service.IProductService;
import com.stackroute.productservice.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("api/product")


public class ProductController {
	
	@Autowired(required = true)
	IProductService iProductService ;
	
	@Autowired
	ImageService imageService;
	
	
	@Value("${project.image}")
	String path ;

	//Adding A Record 
	
	
	
	
	@PostMapping(value="/product",consumes = {"multipart/form-data"})
	
	public ResponseEntity<ProductDto> addNewProduct(@RequestPart(value = "data") ProductDto productDto, @RequestParam(value = "file",required = false) MultipartFile file) throws Exception {
		
		String fileName=null ;
		
	
	
	fileName = this.imageService.uploadImage(path,file) ;
	
	
	productDto.setProductImage(fileName);
	return new ResponseEntity<>(iProductService.addNewProduct(productDto), HttpStatus.OK);
	}

	
	

	//Retrieving product By productId
	@GetMapping("/product/{productId}")
	public ResponseEntity<ProductDto> getProductById(@PathVariable String productId){
	
		return new ResponseEntity<ProductDto>(iProductService.getProductById(productId),HttpStatus.OK) ;
		
	}

	
	//Retrieving all the products
	
	@GetMapping("/product")
	public ResponseEntity<List<ProductDto>> getAllProducts(){
		
		return new ResponseEntity<List<ProductDto>>(iProductService.getAllProducts(),HttpStatus.OK);
	}
	
	
	
	//Deleting the Products by productId
	
	@DeleteMapping("/product/{productId}")
	public ResponseEntity<String> deleteProduct(@PathVariable String productId){
		iProductService.deleteProductById(productId);
		
		return new ResponseEntity<String>("Deleted Successfully",HttpStatus.OK);
		
	}
	
	//Updating a record 
	
	@PutMapping("/product/{productId}")
	public ResponseEntity<ProductDto> updateProduct(@RequestPart(value = "data") ProductDto productDto,@RequestPart(value = "file",required = false) MultipartFile file,@PathVariable String productId)
	throws Exception{
		
				
			String fileName=null ;
		
		
			
		//ProductDto productDto1=this.iProductService.getProductById(productId);
		
		fileName = this.imageService.uploadImage(path,file) ;
		
		
		productDto.setProductImage(fileName);
		ProductDto updateProduct=iProductService.updateProductById(productId, productDto);
		return new ResponseEntity<ProductDto>(updateProduct,HttpStatus.OK);

				
		
	}


}
