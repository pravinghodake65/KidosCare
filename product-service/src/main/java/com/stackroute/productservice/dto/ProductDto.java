package com.stackroute.productservice.dto;

import java.util.UUID;

import org.springframework.data.mongodb.core.mapping.MongoId;


public class ProductDto {

	@MongoId
	private String productId= UUID.randomUUID().toString();
	
	private String productName ;
	private String productCategory;
	private String productDescription ;
	private String mfgDate ;
	private String expDate ;
	private Integer productUnits ;
	private Long mrpPrice ;
	private Long bestPrice ;
	
	private	String productImage ;
	
	
	
	
	public ProductDto() {
		super();
		
	}





	public ProductDto(String productId, String productName, String productCategory, String productDescription,
			String mfgDate, String expDate, Integer productUnits, Long mrpPrice, Long bestPrice, String productImage) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productCategory = productCategory;
		this.productDescription = productDescription;
		this.mfgDate = mfgDate;
		this.expDate = expDate;
		this.productUnits = productUnits;
		this.mrpPrice = mrpPrice;
		this.bestPrice = bestPrice;
		this.productImage = productImage;
	}





	public String getProductId() {
		return productId;
	}





	public void setProductId(String productId) {
		this.productId = productId;
	}





	public String getProductName() {
		return productName;
	}





	public void setProductName(String productName) {
		this.productName = productName;
	}





	public String getProductCategory() {
		return productCategory;
	}





	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}





	public String getProductDescription() {
		return productDescription;
	}





	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}





	public String getMfgDate() {
		return mfgDate;
	}





	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}





	public String getExpDate() {
		return expDate;
	}





	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}





	public Integer getProductUnits() {
		return productUnits;
	}





	public void setProductUnits(Integer productUnits) {
		this.productUnits = productUnits;
	}





	public Long getMrpPrice() {
		return mrpPrice;
	}





	public void setMrpPrice(Long mrpPrice) {
		this.mrpPrice = mrpPrice;
	}





	public Long getBestPrice() {
		return bestPrice;
	}





	public void setBestPrice(Long bestPrice) {
		this.bestPrice = bestPrice;
	}





	public String getProductImage() {
		return productImage;
	}





	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
	
	
	
	
	

		
	

}
