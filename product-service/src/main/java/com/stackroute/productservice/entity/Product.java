/*
 * @Author Karan Kumar
 */

package com.stackroute.productservice.entity;


import java.util.UUID;

import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(value="products")
public class Product{
	
	@MongoId
	private String productId=UUID.randomUUID().toString();  //Act as Primary Key 
	
	@NotBlank
	private String productName ;
	
	@NotBlank
	private String productCategory;
	
	@NotBlank
	private String productDescription ;
	
	@NotBlank
	private String mfgDate ;
	
	@NotBlank
	private String expDate ;
	
	@NotBlank
	private Integer productUnits ;
	
	@NotBlank
	private Long mrpPrice ;
	
	@NotBlank
	private Long bestPrice ;
	
	@NotBlank
	private	String productImage ;
	
	
	

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}




	public Product(String productId, @NotBlank String productName, @NotBlank String productCategory,
			@NotBlank String productDescription, @NotBlank String mfgDate, @NotBlank String expDate,
			@NotBlank Integer productUnits, @NotBlank Long mrpPrice, @NotBlank Long bestPrice,
			@NotBlank String productImage) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productCategory = productCategory;
		this.productDescription = productDescription;
		this.mfgDate = mfgDate;
		this.expDate = expDate;
		this.productUnits = productUnits;
		this.mrpPrice = mrpPrice;
		this.bestPrice = bestPrice;
		this.productImage = productImage;
	}




	public String getProductId() {
		return productId;
	}




	public void setProductId(String productId) {
		this.productId = productId;
	}




	public String getProductName() {
		return productName;
	}




	public void setProductName(String productName) {
		this.productName = productName;
	}




	public String getProductCategory() {
		return productCategory;
	}




	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}




	public String getProductDescription() {
		return productDescription;
	}




	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}




	public String getMfgDate() {
		return mfgDate;
	}




	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}




	public String getExpDate() {
		return expDate;
	}




	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}




	public Integer getProductUnits() {
		return productUnits;
	}




	public void setProductUnits(Integer productUnits) {
		this.productUnits = productUnits;
	}




	public Long getMrpPrice() {
		return mrpPrice;
	}




	public void setMrpPrice(Long mrpPrice) {
		this.mrpPrice = mrpPrice;
	}




	public Long getBestPrice() {
		return bestPrice;
	}




	public void setBestPrice(Long bestPrice) {
		this.bestPrice = bestPrice;
	}




	public String getProductImage() {
		return productImage;
	}




	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	
	
	
	
	
	
	
	
	
	
	


}

