/*
 * @Author Karan Kumar
 */

package com.stackroute.productservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//GLOBAL EXCEPTION LAYER TO HANDLE ALL THE EXCEPTIONS

@RestControllerAdvice
public class GlobalExceptionHandling {
	
	@ExceptionHandler(IdNotFoundException.class)
	public ResponseEntity<String> IdNotFound(IdNotFoundException exception){
		
		return new ResponseEntity<String>(exception.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(PriceException.class)
	public ResponseEntity<String> PriceNotValid(PriceException priceexception){
		
		return new ResponseEntity<String>(priceexception.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ImageNotFoundException.class)
	public ResponseEntity<String> ImageNotFound(ImageNotFoundException imageException){
		return new ResponseEntity<String>(imageException.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	
	
	
	

}
