/*
 * @Author Karan Kumar
 */

package com.stackroute.productservice.exception;

public class ImageNotFoundException extends RuntimeException {
	
	String message ;

	public ImageNotFoundException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
