/*
 * @Author Karan Kumar
 */

package com.stackroute.productservice.exception;

public class PriceException extends RuntimeException {
	
	public String message ;
	
	public PriceException(String message) {
		super();
		this.message=message;
	}
	
	public String getMessage() {
		return message ;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
