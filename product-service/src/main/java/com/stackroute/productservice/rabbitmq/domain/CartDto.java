package com.stackroute.productservice.rabbitmq.domain;

import com.stackroute.productservice.entity.Product;

import java.util.Map;

public class CartDto {
    private Map<String, Product> cartProducts;

    public Map<String, Product> getCartProducts() {
        return cartProducts;
    }

    public void setCartProducts(Map<String, Product> cartProducts) {
        this.cartProducts = cartProducts;
    }

    public CartDto(Map<String, Product> cartProducts) {
        this.cartProducts = cartProducts;
    }
    public CartDto(){
        super();
    }

}
