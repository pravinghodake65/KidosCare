/*
 * @Author Karan Kumar
 */


package com.stackroute.productservice.repository;

import com.stackroute.productservice.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;





public interface ProductRepository extends MongoRepository<Product, String > {
	

}
