/*
 * @Author Karan Kumar
 */


package com.stackroute.productservice.service;

import com.stackroute.productservice.dto.ProductDto;

import java.util.List;




public interface IProductService {
	
	 ProductDto addNewProduct(ProductDto productDto);
	 List<ProductDto> getAllProducts();
	 ProductDto getProductById(String productId);
	 void deleteProductById(String productId);
	 ProductDto updateProductById(String productId,ProductDto productDto);
	
	
	//Image Part 
	//public ProductDto addImage(byte[] productImage);
	
}
