/*
 * @Author Karan Kumar
 */



package com.stackroute.productservice.service;

import java.io.FileNotFoundException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;


public interface ImageService {

	String uploadImage(String path,MultipartFile file) throws Exception;
	
	InputStream getResource(String path,String fileName) throws FileNotFoundException ;
	
	
}
