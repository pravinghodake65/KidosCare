/*
 * @Author Karan Kumar
 */

package com.stackroute.productservice.service;

import java.util.*;

import com.stackroute.productservice.config.Producer;
import com.stackroute.productservice.dto.ProductDto;
import com.stackroute.productservice.entity.Product;
import com.stackroute.productservice.exception.IdNotFoundException;
import com.stackroute.productservice.exception.PriceException;
import com.stackroute.productservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ImplProductService implements IProductService{


	@Autowired
	Producer producer ;
	@Autowired(required = true)
	ProductRepository productRepository ;
	
	/*
	 * @Description : This method is used to perform the addition of products
	 * @Params:It takes Product type as input
	 * @Returns:It returns the products
	 * @Throws: Price Exception if MRP<Best Price
	 * @Created By: Karan Kumar
	 * 
	 */
	
	@Override
	public ProductDto addNewProduct(ProductDto productDto) {

		Map<String, Product> cartDto = new HashMap<>();
		Product product = convertToEntity(productDto);
		cartDto.put(productDto.getProductId(),product);
		cartDto.put(productDto.getProductName(), product);
		cartDto.put(productDto.getProductCategory(), product);
		cartDto.put(productDto.getProductDescription(), product);
		cartDto.put(String.valueOf(productDto.getMrpPrice()), product);
		cartDto.put(String.valueOf(productDto.getBestPrice()), product);
		cartDto.put(productDto.getMfgDate(), product);
		cartDto.put(productDto.getExpDate(), product);
		cartDto.put(productDto.getProductImage(), product);

		if(productDto.getBestPrice()>productDto.getMrpPrice()) {

			throw new PriceException("Best Price : "+productDto.getBestPrice()+" || Must be Lower than Marked Retail Price : "+productDto.getMrpPrice());
		}


		ProductDto products = convertToDto(productRepository.save(convertToEntity(productDto)));

		producer.sendMessageToRabbitMq(cartDto);

		return products ;
	}

	/*
	 * @Description : This method is used to get all the products
	 * @Params:na
	 * @Returns:It returns all products
	 * @Throws: 
	 * @Created By: Karan Kumar
	 * 
	 */
	
	@Override
	public List<ProductDto> getAllProducts() {
		
		List<Product> products=(List<Product>)productRepository.findAll();
		List<ProductDto>productDtos=new ArrayList<>();
		
		for (Product product : products) {
			
			productDtos.add(convertToDto(product));
			
		}
		return productDtos ;
		
	}
	
	/*
	 * @Description : This method is used to get product by productId
	 * @Params:It takes productId type as input
	 * @Returns:It returns the book
	 * @Throws: IdNotFoundException,If Id didn't exists
	 * @Created By: Karan Kumar
	 * 
	 */

	@Override
	
	public ProductDto getProductById(String productId) {
		
		Product product=productRepository.findById(productId).
				orElseThrow(()->new IdNotFoundException("Invalid ProductId : "+productId+ " ||  Please enter the correct ProductId !!! "));
		
		return convertToDto(product);
}
	
	/*
	 * @Description : This method is used to delete product
	 * @Params:It takes productId type as input
	 * @Returns:
	 * @Throws: IdNotFoundException,If Id didn't exists
	 * @Created By: Karan Kumar
	 * 
	 */
	@Override
	public void deleteProductById(String productId) {
		
		Product product=productRepository.findById(productId).
				orElseThrow(()-> new IdNotFoundException("Invalid ProductId: "+productId));
		
		productRepository.delete(product);
		
	}
	
	/*
	 * @Description : This method is used to update the products
	 * @Params:It takes Product type and productId as input
	 * @Returns:It returns the products
	 * @Throws: Price Exception if MRP<Best Price and IdNotFoundException,If Id didn't exists
	 * @Created By: Karan Kumar
	 * 
	 */
	
	@Override
	public ProductDto updateProductById(String productId, ProductDto productDto) {
		Optional<Product> optionalProduct= productRepository.findById(productId);
				if(optionalProduct.isEmpty()) {
					throw new IdNotFoundException("Invalid ProductId : "+productId+ " while uploading || Please entered a valid productId : ");
				}
				
				if(productDto.getBestPrice()>productDto.getMrpPrice()) {
					throw new PriceException("Best Price : "+productDto.getBestPrice()+" || Must be Lower than Marked Retail Price : "+productDto.getMrpPrice());
				}
						
			Product newProduct=	optionalProduct.get();
			newProduct.setProductName(productDto.getProductName());
			newProduct.setProductCategory(productDto.getProductCategory());
			newProduct.setProductDescription(productDto.getProductDescription());
			newProduct.setMfgDate(productDto.getMfgDate());
			newProduct.setExpDate(productDto.getExpDate());
			newProduct.setProductUnits(productDto.getProductUnits());
			newProduct.setMrpPrice(productDto.getMrpPrice());
			newProduct.setBestPrice(productDto.getBestPrice());
			newProduct.setProductImage(productDto.getProductImage());
			
			
		return 	convertToDto(productRepository.save(newProduct));
			
				
	}
	
	
	//Convert Product to ProductDto
	
	public ProductDto convertToDto(Product product) {
		
		
		
		ProductDto productDto=new ProductDto();
		
		productDto.setProductId(product.getProductId());
		productDto.setProductName(product.getProductName());
		productDto.setProductCategory(product.getProductCategory());
		productDto.setProductDescription(product.getProductDescription());
		productDto.setProductUnits(product.getProductUnits());
		productDto.setMrpPrice(product.getMrpPrice());
		productDto.setBestPrice(product.getBestPrice());
		productDto.setMfgDate(product.getMfgDate());
		productDto.setExpDate(product.getExpDate());
		productDto.setProductImage(product.getProductImage());
		
		return productDto ;
		
		
		
	}
	
	//Convert ProductDto to Product
public Product convertToEntity(ProductDto productDto) {
		
		Product product=new Product();
		
		product.setProductId(productDto.getProductId());
		product.setProductName(productDto.getProductName());
		product.setProductCategory(productDto.getProductCategory());
		product.setProductDescription(productDto.getProductDescription());
		product.setProductUnits(productDto.getProductUnits());
		product.setMrpPrice(productDto.getMrpPrice());
		product.setBestPrice(productDto.getBestPrice());
		product.setMfgDate(productDto.getMfgDate());
		product.setExpDate(productDto.getExpDate());
		product.setProductImage(productDto.getProductImage());
		
		return product ;

}







}
