package com.stackroute.rabbitmq.domain;

import com.stackroute.userservice.dto.AddressDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CartDto {
    private String userEmail;
    private AddressDto address;
}
