package com.stackroute.userservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class  MessageConfiguration {
    public String exchangeName="user_exchange";
    public String register_queue = "user_queue";
    public String cartExchange = "cart_exchange";
    public static final String cart_queue = "cart_queue";
    public static final String seller_queue = "seller_queue";
    public static final String seller_routing_key = "seller_routing_key";
    public String sellerExchange = "seller_exchange";
    public static final String cart_route_key = "cart_route_key";

    @Bean
    public DirectExchange directExchange()
    {
        return new DirectExchange(exchangeName);
    }

    @Bean
    public TopicExchange cartExchange(){return new TopicExchange(cartExchange);}

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter()
    {
        return new  Jackson2JsonMessageConverter();
    }
    @Bean
    public Queue registerQueue() {
        return new Queue(register_queue, false);
    }
    @Bean
    public Queue cartQueue(){
        return new Queue(cart_queue, false,false,false);
    }

    @Bean
    public Queue sellerQueue(){return new Queue(seller_queue, false, false, false);}
    @Bean
    Binding bindingUser(Queue registerQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(registerQueue()).to(directExchange).with("user_routing_key");
    }
    @Bean
    Binding bindingCart(Queue cartQueue, TopicExchange cartExchange) {
        return BindingBuilder.bind(cartQueue()).to(cartExchange).with(cart_route_key);
    }
    @Bean
    Binding bindingSeller(Queue sellerQueue, TopicExchange sellerExchange) {
        return BindingBuilder.bind(sellerQueue()).to(sellerExchange).with(seller_routing_key);
    }
    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

}

