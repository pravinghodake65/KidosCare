package com.stackroute.userservice.config;

import com.stackroute.rabbitmq.domain.CartDto;
import com.stackroute.rabbitmq.domain.UserDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    Log log = LogFactory.getLog(Producer.class);
    private RabbitTemplate rabbitTemplate;
    private DirectExchange exchange;
    private TopicExchange cartExchange;

    @Autowired
    public Producer(RabbitTemplate rabbitTemplate, DirectExchange exchange, TopicExchange cartExchange) {
        super();
        this.rabbitTemplate = rabbitTemplate;
        this.exchange = exchange;
        this.cartExchange = cartExchange;
    }

    public void sendMessageToRabbitMq(UserDto userDTO)
    {
        rabbitTemplate.convertAndSend(exchange.getName(),"user_routing",userDTO);
        log.info("Data send from user");
    }
    public void sendMessageToCart(CartDto cartDto){
        rabbitTemplate.convertAndSend(cartExchange.getName(), MessageConfiguration.cart_route_key, cartDto);
        log.info("Email Id send to cart");
    }

    public void sendMessageToProduct(UserDto userDto){
        rabbitTemplate.convertAndSend(cartExchange.getName(), MessageConfiguration.seller_routing_key, userDto);
        log.info("Seller details send to product");
    }
}

