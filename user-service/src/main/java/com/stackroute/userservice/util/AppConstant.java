package com.stackroute.userservice.util;

public class AppConstant {
    public static final String DELETE_MESSAGE = "Delete Successful";
    public static final String USER_NOT_FOUND_MESSAGE = "User details are not available for  given user email ";
    public static final String USER_ALREADY_EXISTS = "User is already available, use another email id to continue";
    public static final String USER_NAME_ALREADY_EXISTS = "User Name is already available, use another username to continue";
    public static final String USER_NOT_EXISTS = "Currently No Users are Available";
}
