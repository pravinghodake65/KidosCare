package com.stackroute.userservice;

import com.stackroute.userservice.dto.AddressDto;
import com.stackroute.userservice.dto.UserDto;
import com.stackroute.userservice.dto.UserUpdateRequest;
import com.stackroute.userservice.entity.Address;
import com.stackroute.userservice.entity.User;
import com.stackroute.userservice.entity.UserRole;
import com.stackroute.userservice.repository.UserRepository;
import com.stackroute.userservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceApplicationTests {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    /**
     * Tests get all User Details
     */
    @Test
    public void testGetAllUserDetails() {

        List<User> userList = new ArrayList<>();

        Address address1 = new Address(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);
        Address address2 = new Address(2, 12, "Motipur complex", "High Court  road", "G.M Road", "vennala", "kochi", "kerala", 335647);
        userList.add(new User("1", "manisha@gmail.com", "Manisha Kumari", "Mg@12345", "9876543210", "8907654321", UserRole.ROLE_SELLER, address1));
        userList.add(new User("1", "shreya@gmail.com", "Shreya Gupta", "Sg@800297", "9456234516", "81234567890", UserRole.ROLE_BUYER, address2));

        when(userRepository.findAll()).thenReturn(userList);
        assertEquals(2, userService.fetchAllUsers().size());
    }

    /**
     * Tests Add User Details or Register User
     */
    @Test
    public void testAddUserDetails() {

        Address address = new Address(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);

        User user = new User("1", "manisha@gmail.com", "Manisha Kumari", "Mg@12345", "9876543210", "8907654321", UserRole.ROLE_SELLER, address);
        UserDto userDto = userService.convertToDto(user);

        when(userRepository.save(user)).thenReturn(user);
        assertEquals(user.getEmailId(), userDto.getEmailId());
    }

    /**
     * Tests Update User Details by User Email Id
     */
    @Test
    public void testUpdateUserDetailsByEmailId() {

        String emailId = "manisha@gmail.com";

        AddressDto address = new AddressDto(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);
        UserUpdateRequest userUpdateRequest = new UserUpdateRequest("Manisha Kumari", "9876543210", "8907654321", address);

        User user = new ModelMapper().map(userUpdateRequest, User.class);
        when(userRepository.findByEmailId(emailId)).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(user)).thenReturn(user);
        assertNotNull(userService.updateUserByEmailId(emailId, userUpdateRequest));
    }

    /**
     * Tests get User Details by user  email Id
     */
    @Test
    public void testGetUserDetailsByEmailId() {
        String emailId = "manisha@gmail.com";

        AddressDto addressDto = new AddressDto(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);
        Optional<UserDto> userDto = Optional.of(new UserDto("1", "manisha@gmail.com", "Manisha Kumari", "Mg@12345", "9876543210", "8907654321", UserRole.ROLE_SELLER, addressDto));

        Address address = new Address(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);
        Optional<User> user = Optional.of(new User("1", "manisha@gmail.com", "Manisha Kumari", "Mg@12345", "9876543210", "8907654321", UserRole.ROLE_SELLER, address));
//		User user = new ModelMapper().map(userDto, User.class);
//		User user = userService.convertToEntity(userDto.get());

        when(userRepository.findByEmailId(emailId)).thenReturn(user);
        assertNotNull(userService.fetchUserByEmailId(emailId));
    }

    /**
     * Tests delete user details bu user  email id
     */
    @Test
    public void testDeleteUserDetailsByEmailId() {

        AddressDto addressDto = new AddressDto(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);

        Optional<UserDto> userDto = Optional.of(new UserDto("1", "manisha@gmail.com", "Manisha Kumari", "Mg@12345", "9876543210", "8907654321", UserRole.ROLE_SELLER, addressDto));
        Address address = new Address(1, 10, "kulhariya complex", "boring road", "gandhi maidan", "stadium", "delhi", "delhi", 657890);
        Optional<User> user = Optional.of(new User("1", "manisha@gmail.com", "Manisha Kumari", "Mg@12345", "9876543210", "8907654321", UserRole.ROLE_SELLER, address));

        when(userRepository.findByEmailId("manisha@gmail.com")).thenReturn(user);
        userService.deleteUserByEmailId(user.get().getEmailId());
    }

}
